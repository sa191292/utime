﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;
using DAL;
using BAL;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Drawing;

namespace UTimeAPI.Controllers
{
    public class AccountController : ApiController
    {

        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        

        [HttpPost]
        public IHttpActionResult Post()
        {
            string retVal = string.Empty;
            
            
            MasterUpdate objmaster = new MasterUpdate();
            if (!objmaster.CheckAlreadyExistUser(HttpContext.Current.Request.Form["UserName"]) && !objmaster.CheckAlreadyExistUser(HttpContext.Current.Request.Form["Email"]))
            {
                HttpFileCollection UploadFile = HttpContext.Current.Request.Files;
                string Image = "";


                for (int i = 0; i < UploadFile.Count; i++)
                {

                    var files = SaveImage(UploadFile[i]);
                    Image = files;


                }
               // string strImages =Base64ToImage(HttpContext.Current.Request.Form["ProfileImage);

                var obj = objmaster.SaveUserDetails(0, 2, HttpContext.Current.Request.Form["UserName"].ToString(), HttpContext.Current.Request.Form["UserPassword"].ToString(), HttpContext.Current.Request.Form["Dob"].ToString(), HttpContext.Current.Request.Form["Gender"].ToString(), HttpContext.Current.Request.Form["Latitude"].ToString(), HttpContext.Current.Request.Form["Longitude"].ToString(), Image, Convert.ToBoolean(HttpContext.Current.Request.Form["IsTermConditions"].ToString()), HttpContext.Current.Request.Form["status"].ToString(), HttpContext.Current.Request.Form["Name"].ToString(), HttpContext.Current.Request.Form["LoginType"].ToString(), HttpContext.Current.Request.Form["DeviceId"].ToString(), HttpContext.Current.Request.Form["Email"].ToString());
                if (obj != null)
                {
                    try
                    {
                        retVal = GetSerialized(obj);
                        retVal = ResponseOk.Replace("[message]", retVal);
                    }
                    catch (Exception ex)
                    {
                        retVal = ResponseErr.Replace("\\", "") + ex.Message;
                    }
                }
                else
                {
                    retVal = ResponseErr.Replace("error occurred", "User is not registered Successfully!").Replace("\\", "");
                }
            }
            else
            {
                retVal = ResponseErr.Replace("error occurred", "Username  Or Email Already Registered!").Replace("\\", "");
          
            }

            return new RawJsonActionResult(retVal);
        }


        private string SaveImage(HttpPostedFile f)
        {
            string strImageName = "";
            string strExtension = "";

            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "\\ApplicationImages\\");

            strExtension = System.IO.Path.GetExtension(f.FileName);
            strImageName = Guid.NewGuid().ToString() + strExtension;

            foreach (FileInfo file in downloadedMessageInfo.GetFiles().Where(x => x.Name == strImageName))
            {
                try
                {

                    file.Delete();
                }
                catch
                {

                }
            }

            f.SaveAs(System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "\\ApplicationImages\\" + strImageName);
           
            return strImageName;

        }
        //[NonAction]
        //public string Base64ToImage(string base64String)
        //{
        //    byte[] imageBytes = Convert.FromBase64String(base64String);
        //    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
        //    ms.Write(imageBytes, 0, imageBytes.Length);
        //    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
        //   string strImageName = Guid.NewGuid().ToString() + ".jpg";
        //   image.Save(System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "ApplicationImages\\" + strImageName);
        //    return strImageName;
        //}


        

    }
}
