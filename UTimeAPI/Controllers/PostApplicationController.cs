﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAL;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class PostApplicationController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        [HttpPost]
        public IHttpActionResult Post(PostApplicationRequestModel model)
        {
            string retVal = string.Empty;

            MasterUpdate objmaster = new MasterUpdate();

            try
            {
               var getPostApps =objmaster.PostApplication(model.UserId,model.AppId);

               retVal = GetSerialized(getPostApps);
                retVal = ResponseOk.Replace("[message]", retVal);
                //retVal = ResponseOk.Replace("success", "Post Application save Successfully!").Replace("[message]", "[]").Replace("\\", "");
            }
            catch (Exception ex)
            {
                retVal = ResponseErr.Replace("\\", "") + ex.Message;
            }


            return new RawJsonActionResult(retVal);
        }
    }
}
