﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using BAL;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class CategoryController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }

        [HttpGet]
        public IHttpActionResult Get(int userId)
        {
            string retVal = string.Empty;
            MasterUpdate objmaster = new MasterUpdate();

            var cateList = objmaster.GetAllCategories(userId);
            if (cateList.Count>0)
            {
                try
                {

                    retVal = GetSerialized(cateList);
                    retVal = ResponseOk.Replace("[message]", retVal);
                }
                catch (Exception ex)
                {
                    retVal = ResponseErr.Replace("\\", "") + ex.Message;
                }
            }


            return new RawJsonActionResult(retVal);
        }
    }
}
