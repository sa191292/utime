﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAL;
using DAL;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class LoginController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        [HttpPost]
        public IHttpActionResult Post(LoginModel loginModel)
        {
         
            string retVal = string.Empty;
            MasterUpdate objmaster = new MasterUpdate();
            
           UserMaster obj = objmaster.Login(loginModel.UserName, loginModel.UserPassword, loginModel.LoginType,2,loginModel.DeviceId);
            if (obj != null)
            {
                try
                {
                    retVal = GetSerialized(obj);
                    retVal = ResponseOk.Replace("[message]", retVal);
                }
                catch (Exception ex)
                {
                    retVal = ResponseErr.Replace("\\", "") + ex.Message;
                }
            }
            else
            {
                retVal = ResponseErr.Replace("error occurred", "Invalid Login Name or Password!").Replace("\\", "");
                //retVal = ResponseOk.Replace("success", "Invalid Login Name or Password!").Replace("[message]", "[]").Replace("\\", "");
            }

            return new RawJsonActionResult(retVal);
        }

    }
}
