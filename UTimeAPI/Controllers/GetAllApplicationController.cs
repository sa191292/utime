﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAL;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class GetAllApplicationController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }

        [HttpGet]
        public IHttpActionResult Get(int userId)
        {
            
            string retVal = string.Empty;
            MasterUpdate objmaster = new MasterUpdate();

            var appList = objmaster.GetAllApplicationList(userId);
            if (appList.Count > 0)
            {
                try
                {

                    retVal = GetSerialized(appList);
                    retVal = ResponseOk.Replace("[message]", retVal);
                }
                catch (Exception ex)
                {
                    retVal = ResponseErr.Replace("\\", "") + ex.Message;
                }
            }
            else
            {
                retVal = ResponseErr.Replace("error occurred", "No Data found!").Replace("\\", "");
            }

            return new RawJsonActionResult(retVal);
        }
    }
}
