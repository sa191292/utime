﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class PostScheduleTimeController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        [HttpPost]
        public IHttpActionResult Post(SavePostScheduleTimeDto model)
        {
            string retVal = string.Empty;

            MasterUpdate objmaster = new MasterUpdate();

            try
            {
                List<ApplicationMaster> findAppCategoryList = new List<ApplicationMaster>();
                foreach (var apps in model.SelectedApps)
                {
                    ApplicationMaster objAppCategory = new ApplicationMaster();
                  objAppCategory  =objmaster.GetApplicationById(apps.AppId);
                  if (objAppCategory != null)
                  {
                    findAppCategoryList.Add(objAppCategory);    
                  }
                }

                foreach (var category in model.CategoryWithTime)
                {

                    if (findAppCategoryList.Where(x => x.CategoryId == category.CategoryId).Count()>1)
                    {
                        retVal = ResponseErr.Replace("error occurred", "Please select one App for one Category to Make Schedule.").Replace("\\", "");
                        return new RawJsonActionResult(retVal);
                    }
                    
                }
                objmaster.PostScheduleTime(model);

                retVal = ResponseOk.Replace("success", "Post Schedule Time saved Successfully!").Replace("[message]", "[]").Replace("\\", "");
            }
            catch (Exception ex)
            {
                retVal = ResponseErr.Replace("\\", "") + ex.Message;
            }


            return new RawJsonActionResult(retVal);
        }
    }
}

