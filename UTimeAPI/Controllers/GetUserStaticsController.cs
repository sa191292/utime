﻿using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class GetUserStaticsController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }

        [HttpGet]
        public IHttpActionResult Get(int userId, string date,string option)
        {

            string retVal = string.Empty;
            MasterUpdate objmaster = new MasterUpdate();

            var getUserStaticsDetails = objmaster.GetUserStaticsData(userId, date,option);
            if (getUserStaticsDetails.Count>0)
            {
                try
                {

                    retVal = GetSerialized(getUserStaticsDetails);
                    retVal = ResponseOk.Replace("[message]", retVal);
                }
                catch (Exception ex)
                {
                    retVal = ResponseErr.Replace("\\", "") + ex.Message;
                }
            }
            else
            {
                retVal = ResponseErr.Replace("error occurred", "No Data found!").Replace("\\", "");
            }

            return new RawJsonActionResult(retVal);
        }

    }
}
