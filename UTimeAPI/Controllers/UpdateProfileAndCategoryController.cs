﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAL;
using System.Web.Script.Serialization;
using System.Web;
using UTimeAPI.Models;
using System.IO;
using Newtonsoft.Json;

namespace UTimeAPI.Controllers
{
    public class UpdateProfileAndCategoryController : ApiController
    {

        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }




        [HttpPost]
        public IHttpActionResult Post()
        {
            string retVal = string.Empty;


            MasterUpdate objmaster = new MasterUpdate();
            //CategoryDto obj = new CategoryDto();
          string obj1= Convert.ToString(HttpContext.Current.Request.Form["category"]);  // "[{  \"categoryId\": 1,  \"subCategoryId\": [{  \"subCategoyId\": 1},{  \"subCategoyId\": 2}]},{  \"categoryId\": 2,  \"subCategoryId\": [{  \"subCategoyId\": 3},{  \"subCategoyId\": 4}]}]";
          bool chkpass = false;
          if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form["NewPassword"]))
          {
              chkpass = true;
          }
          if (objmaster.MatchOldPassword(Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]), Convert.ToString(HttpContext.Current.Request.Form["OldPassword"])) == true || chkpass==false)
            {
                HttpFileCollection UploadFile = HttpContext.Current.Request.Files;
                string Image = "";


                for (int i = 0; i < UploadFile.Count; i++)
                {

                    var files = SaveImage(UploadFile[i]);
                    Image = files;

                }

                CategoryDto2 categJson = JsonConvert.DeserializeObject<CategoryDto2>(obj1.ToString());
                objmaster.UpdateCategoryAndProfile(Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]),Image,
                    HttpContext.Current.Request.Form["OldPassword"].ToString(), HttpContext.Current.Request.Form["NewPassword"].ToString()
                    , categJson, Convert.ToString(HttpContext.Current.Request.Form["Dob"]), Convert.ToString(HttpContext.Current.Request.Form["Gender"]), Convert.ToString(HttpContext.Current.Request.Form["Latitude"]), Convert.ToString(HttpContext.Current.Request.Form["Longitude"]), Convert.ToString(HttpContext.Current.Request.Form["name"]), Convert.ToString(HttpContext.Current.Request.Form["username"]));
               
                    try
                    {
                       var getUserDetails =objmaster.GetUserDetailById(Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]));
                       retVal = GetSerialized(getUserDetails);
                        retVal = ResponseOk.Replace("[message]", retVal);
                        //retVal = ResponseOk.Replace("success", "Profile updated Successfully!").Replace("[message]", "[]").Replace("\\", "");
                    }
                    catch (Exception ex)
                    {
                        retVal = ResponseErr.Replace("\\", "") + ex.Message;
                    }
                
            }
            else
            {
                retVal = ResponseErr.Replace("error occurred", "Old Password does not match!").Replace("\\", "");

            }

            return new RawJsonActionResult(retVal);
        }


        private string SaveImage(HttpPostedFile f)
        {
            string strImageName = "";
            string strExtension = "";

            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "\\ApplicationImages\\");

            strExtension = System.IO.Path.GetExtension(f.FileName);
            strImageName = Guid.NewGuid().ToString() + strExtension;

            foreach (FileInfo file in downloadedMessageInfo.GetFiles().Where(x => x.Name == strImageName))
            {
                try
                {

                    file.Delete();
                }
                catch
                {

                }
            }

            f.SaveAs(System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "\\ApplicationImages\\" + strImageName);

            return strImageName;

        }
    }
}
