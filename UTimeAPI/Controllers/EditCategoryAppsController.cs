﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class EditCategoryAppsController : ApiController
    {

        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        [HttpPost]
        public IHttpActionResult Post(EditApplicationRequestModel userApps)
        {
            string retVal = string.Empty;

            MasterUpdate objmaster = new MasterUpdate();

            //try
            //{
            var getEditUserAppsList = objmaster.EditCategoryApps(userApps.UserId, userApps.AppId,userApps.CategoryId);

            if (getEditUserAppsList.Count > 0)
            {
                try
                {

                    retVal = GetSerialized(getEditUserAppsList);
                    retVal = ResponseOk.Replace("[message]", retVal);
                }
                catch (Exception ex)
                {
                    retVal = ResponseErr.Replace("\\", "") + ex.Message;
                }
            }

            //    retVal = ResponseOk.Replace("success", "User Application updated Successfully!").Replace("[message]", "[]").Replace("\\", "");
            //}
            //catch (Exception ex)
            //{
            //    retVal = ResponseErr.Replace("\\", "") + ex.Message;
            //}


            return new RawJsonActionResult(retVal);
        }
    }
}
