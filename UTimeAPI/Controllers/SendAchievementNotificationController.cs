﻿using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class SendAchievementNotificationController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;


            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }

        [HttpGet]
        public IHttpActionResult Get()
        {

            string retVal = string.Empty;
            MasterUpdate objmaster = new MasterUpdate();

            try
            {

                objmaster.SendNotificationForGoalAchievements();
                retVal = ResponseOk.Replace("success", "User Achievement Notification have been send successfully. ").Replace("[message]", "[]").Replace("\\", "");
            }
            catch (Exception ex)
            {
                retVal = ResponseErr.Replace("\\", "") + ex.Message;
            }

            return new RawJsonActionResult(retVal);
        }
    }
}
