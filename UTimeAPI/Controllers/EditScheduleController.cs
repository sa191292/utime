﻿using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UTimeAPI.Models;

namespace UTimeAPI.Controllers
{
    public class EditScheduleController : ApiController
    {
        string ResponseOk = "{\"status\": \"1\", \"message\": \"success\", \"data\":[message]}";
        string ResponseErr = "{\"status\": \"0\", \"message\": \"error occurred\"}";
        private string GetSerialized(object obj)
        {
            //var serializer = new JavaScriptSerializer() ;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return javaScriptSerializer.Serialize(obj);

        }


        [HttpPost]
        public IHttpActionResult Post(int scheduleId,string scheduleTime,int appId)
        {
            string retVal = string.Empty;

            MasterUpdate objmaster = new MasterUpdate();

            try
            {
               bool checkScheduleTime =objmaster.CheckExistScheduleTime(scheduleId, scheduleTime,appId);

               if (checkScheduleTime == true)
               {
                   retVal = ResponseErr.Replace("error occurred", "Please select Another Time to Make Schedule.").Replace("\\", "");
               }
               else
               {
                   objmaster.EditScheduleDetails(scheduleId, scheduleTime, appId);

                   retVal = ResponseOk.Replace("success", "Schedule edited Successfully!").Replace("[message]", "[]").Replace("\\", "");
               }
                
            }
            catch (Exception ex)
            {
                retVal = ResponseErr.Replace("\\", "") + ex.Message;
            }


            return new RawJsonActionResult(retVal);
        }
    
    }
}
