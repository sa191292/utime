﻿using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTimeAPI.Models
{
    public class RequestModel
    {
        public string UserName { get; set; }

    }

    public class PostApplicationRequestModel
    {
        public int UserId { get; set; }

        public int AppId { get; set; }
    }

    public class EditApplicationRequestModel
    {
        public int UserId { get; set; }

        public List<SelectedAppsDto> AppId;

        public int CategoryId { get; set; }
    }
}