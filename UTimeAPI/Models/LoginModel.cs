﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTimeAPI.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        public string LoginType { get; set; }

        public string DeviceId { get; set; }
    }
}