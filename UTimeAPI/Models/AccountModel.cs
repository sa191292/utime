﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTimeAPI.Models
{
    public class AccountModel
    {
        public string UserName { get; set; }

        public string UserPassword { get; set; }
        public string Dob { get; set; }
        public string Gender { get; set; }
        public string Latitude { get; set; }

        public string Longitude { get; set; }
        public string ProfileImage { get; set; }

        public bool IsTermConditions { get; set; }

        public string status { get; set; }
        public string Name { get; set; }
    }
}