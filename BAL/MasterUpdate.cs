﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Net.Mail;
using System.Net;
using PushSharp.Apple;
using Newtonsoft.Json.Linq;
using System.IO;


namespace BAL
{
    public class MasterUpdate : BusinessBase
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["ImagePath"];
        string logPath = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
        public UserMaster Login(string emailAddress, string password, string loginType, int roleId,string deviceId)
        {
            UserMaster usermaster = new UserMaster();
            usermaster = (from um in DB.UserMasters where (um.UserName == emailAddress || um.UserEmail == emailAddress) && um.UserPassword == password && um.RoleId == roleId select um).FirstOrDefault();
            if (usermaster != null)
            {
                if (!string.IsNullOrEmpty(deviceId))
                {
                    var userDeviceToken = usermaster.DeviceId == null ? "" : usermaster.DeviceId;
                    var existToken = userDeviceToken.Contains(deviceId);
                  if (existToken == false)
                  {
                      //var existMultipleToken = usermaster.DeviceId.Contains(',');
                      usermaster.DeviceId = !string.IsNullOrEmpty(usermaster.DeviceId) ? usermaster.DeviceId + ',' + deviceId : deviceId;
                  }
                  
                    DB.SubmitChanges();
                }
            }

            return usermaster;
        }

        public UserMaster SaveUserDetails(int userId, int roleId, string userName, string userpassword, string dob, string gender, string lat,
            string longt, string profileImg, bool isTerms, string status, string name, string loginType,string deviceId,string email)
        {
            UserMaster um = new UserMaster();
            if (userId == 0)
            {
                um.RoleId = roleId;
                um.UserName = userName;
                um.UserPassword = userpassword;
                um.Dob = dob;
                um.Gender = gender;
                um.Latitude = lat;
                um.Longitude = longt;
                um.ProfileImage = profileImg;
                um.IsTermConditions = isTerms;
                um.UserStatus = status;
                um.DeviceId = deviceId;
                um.CreatedDate = DateTime.Now;
                um.Name = name;
                if (email != "")
                {
                    um.UserEmail = email;
                }
                
                um.LoginType = loginType;
                DB.UserMasters.InsertOnSubmit(um);
            }
            else
            {
                um = (from u in DB.UserMasters where u.Id == userId select u).SingleOrDefault();
                um.RoleId = roleId;
                um.UserName = userName;
                if (userpassword != "")
                    um.UserPassword = userpassword;

                if (email != "")
                {
                    um.UserEmail = email;
                }
                um.Dob = dob;
                um.Gender = gender;
                um.Latitude = lat;
                um.Longitude = longt;
                if (!string.IsNullOrEmpty(profileImg))
                um.ProfileImage = profileImg;
                um.IsTermConditions = isTerms;
                um.UserStatus = status;
                um.Name = name;
                um.ModifyDate = DateTime.Now;
            }
            DB.SubmitChanges();
            return um;
        }


        public bool CheckAlreadyExistUser(string userName)
        {
            var objCheck = (from exist in DB.UserMasters where (exist.UserName == userName || exist.UserEmail == userName) select exist).Count();

            if (objCheck == 0)
            {
                return false;
            }
            return true;
        }

        public List<CategoryListDto> GetAllCategories(int userId)
        {
            return (from cat in DB.CategoryMasters
                    select new CategoryListDto
                    {
                        CategoryId = cat.Id,
                        CategoryName = cat.CategoryName,
                        CategoryImage = path + cat.CategoryImage,
                        IsSelected = IsUserSelectedCategory(userId, cat.Id),
                        SubCategory = (from subCate in DB.SubCategoryMasters
                                       where subCate.CategoryId == cat.Id
                                       select new SubCategoryListDto
                                       {
                                           SubCategoryName = subCate.SubCategoryName,
                                           SubCategoryId=subCate.Id,
                                           IsSelected = IsUserSelectedSubCategory(userId, subCate.Id)
                                       }).ToList()
                    }).ToList();
        }


        public bool IsUserSelectedCategory(int userId,int cateId)
        {
           var getUserCategory =(from cate in DB.CategoryWithSubCategoryLinks where cate.CategoryId == cateId && cate.UserId ==userId select cate).FirstOrDefault();

           if (getUserCategory != null)
           {
               return true;
           }
           return false;
        }
        public bool IsUserSelectedSubCategory(int userId, int SubcateId)
        {
            var getUserCategory = (from cate in DB.CategoryWithSubCategoryLinks where cate.SubCategoryId == SubcateId && cate.UserId == userId select cate).FirstOrDefault();

            if (getUserCategory != null)
            {
                return true;
            }
            return false;
        }

        public CategoryMaster AddCategory(int Id, string CateName, string CateImg)
        {
            CategoryMaster cm = new CategoryMaster();
            if (Id == 0)
            {
                cm.CategoryName = CateName;
                cm.CategoryImage = CateImg;
                cm.CreatedDate = DateTime.Now;
                DB.CategoryMasters.InsertOnSubmit(cm);
            }
            else
            {
                cm = (from cate in DB.CategoryMasters where cate.Id == Id select cate).SingleOrDefault();
                cm.CategoryName = CateName;
                if(!string.IsNullOrEmpty(CateImg))
                cm.CategoryImage = CateImg;
                cm.ModifiedDate = DateTime.Now;
            }

            DB.SubmitChanges();

            return cm;
        }

        public CategoryMaster GetCategoryById(int id)
        {
            return (from cat in DB.CategoryMasters where cat.Id == id select cat).SingleOrDefault();
        }

        public List<CategoryMaster> GetAllCategoryList()
        {
            return (from cateList in DB.CategoryMasters select cateList).ToList();
        }

        public void DeleteCategory(int id)
        {
            var getCate = GetCategoryById(id);
            DB.CategoryMasters.DeleteOnSubmit(getCate);
            DB.SubmitChanges();
        }

        public bool ForgotPassword(string email)
        {
            var d = (from t in DB.UserMasters where (t.UserName == email || t.UserEmail ==email) select t).SingleOrDefault();
            if (d != null)
            {
                Common.SendEMail(d.UserEmail, "Forgot Password", "Dear " + d.Name + "<br /><br />Your password is " + d.UserPassword + "<br /> <br /> Thanks");
                return true;
            }
            else
            {
                return false;
            }
        }


        public static void SendMail(string toMail, string subject, string body = "", string attpath = "")
        {
            SmtpClient ss = new SmtpClient();

            try
            {
                //ss.Host = "mail.unipoi.com";
                ss.Host = "smtp.domain.com";

                ss.Port = 25;
                ss.Timeout = 10000;
                ss.DeliveryMethod = SmtpDeliveryMethod.Network;
                ss.UseDefaultCredentials = false;
                ss.Credentials = new NetworkCredential("info@unipoi.com", "Unipoi=20152");
                if (body == "")
                {
                    //body = "Your password has been changed. Your new password is "+;
                }
                //"The Smart Plan Dating Network"
                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add(toMail);
                mailMsg.From = new MailAddress("info@unipoi.com");
                mailMsg.Subject = subject;
                mailMsg.Body = body;
                mailMsg.IsBodyHtml = true;
                if (attpath != "")
                {
                    Attachment a = new Attachment(attpath);
                    mailMsg.Attachments.Add(a);
                }
                mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                ss.Send(mailMsg);

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                //Console.ReadKey();
            }
        }

        public void SaveUserCateoryWithSubCategory(PostUserCategoryDto userCategory)
        {
            CategoryWithSubCategoryLink obj;
            var dd = (from t in DB.SubCategoryMasters select t).ToList();
            foreach (var getCate in userCategory.SubCategoryIds)
            {
                var catg=dd.Where(x=>x.Id==Convert.ToInt32(getCate)).FirstOrDefault();
                
                obj = new CategoryWithSubCategoryLink();
                obj.UserId = userCategory.UserId;
                if (catg != null)
                {
                    obj.CategoryId = catg.CategoryId;
                }
                obj.SubCategoryId = Convert.ToInt32(getCate);
                obj.CreatedDate = DateTime.Now;
                DB.CategoryWithSubCategoryLinks.InsertOnSubmit(obj);
                DB.SubmitChanges();
            }

        }

        public UserProfileDto GetUserProfile(int userId)
        {
            var goalList = (from goal in DB.SetGoalsDuringWeeks
                            join cate in DB.CategoryMasters on goal.CategoryId equals cate.Id
                            where goal.UserId == userId
                            select new 
                            {
                                UserId=goal.UserId,
                                Daily=goal.Daily,
                                Weekly=goal.Weekly,
                                CategoryId=goal.CategoryId,
                                NoOfDays = goal.NoOfDays,
                                 CategoryName=cate.CategoryName   
                            }
                ).ToList();
            var goalCategoryList=goalList.Where(x=>x.UserId==userId).Select(y=> new SetGoalCategoryListClass{
               CategoryId=(int) y.CategoryId,
               CategoryName=y.CategoryName,
               NoOfDays=(int) y.NoOfDays
            }).ToList();

            return (from um in DB.UserMasters
                    where um.Id == userId
                    select new UserProfileDto
                    {
                        Name = um.Name,
                        Password = um.UserPassword,
                        ProfileImage = path + um.ProfileImage,
                        Categories = (from cate in DB.CategoryMasters
                                      join userCate in DB.CategoryWithSubCategoryLinks on cate.Id equals userCate.CategoryId
                                      where userCate.UserId == userId
                                      select new UserCategoryListDto
                                      {
                                          CategoryId = cate.Id,
                                          CategoryName = cate.CategoryName,
                                          CategoryImage = path + cate.CategoryImage
                                      }).Distinct().ToList(),
                        Gender = um.Gender,
                        BirthDay = um.Dob,
                        Latitude = um.Latitude,
                        Longitude = um.Longitude,
                        NoOfDaysUsingApp = DaysBetween(Convert.ToDateTime(um.CreatedDate), DateTime.Now),
                        Level = "Beginner",
                        SetGoalDuringWeek = goalList.Select(x => new SetGoalClass { 
                            Daily=x.Daily,
                            Weekly=x.Weekly,
                            CategoryWithDaysList = goalCategoryList
                        }).FirstOrDefault()

                    }).SingleOrDefault();
        }

        int DaysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            return (int)span.TotalDays;
        }


        public List<UserCategoryListDto> GetUserCategory(int userId)
        {

            return (from cate in DB.CategoryMasters
                    join userCate in DB.CategoryWithSubCategoryLinks on cate.Id equals userCate.CategoryId
                    where userCate.UserId == userId
                    select new UserCategoryListDto
                    {
                        CategoryId = cate.Id,
                        CategoryName = cate.CategoryName
                    }).Distinct().ToList();

        }

        public List<ApplicationMasterWithCat> GetAllApplicationList(int userId)
        {
            return (from app in DB.ApplicationMasters join catLink in DB.CategoryWithSubCategoryLinks
                       on app.CategoryId equals catLink.CategoryId
                    join catLinkname in DB.CategoryMasters
                   on app.CategoryId equals catLinkname.Id
                    where catLink.UserId == userId 
                     select new ApplicationMasterWithCat { 
                Id=app.Id,
                CategoryName = catLinkname.CategoryName,
                AppId=app.AppId,
                AppName=app.AppName,
                AppLink=app.AppLink,
                AppStoreLink=app.AppStoreLink,
                CreatedDate = (DateTime)app.CreatedDate,
                     CategoryId=app.CategoryId
                     
                     }).Distinct().ToList();
        }

        public PostApplication PostApplication(int userId, int appId)
        {
            PostApplication objPostApplication = new PostApplication();
            objPostApplication.UserId = userId;
            objPostApplication.AppId = appId;
            DB.PostApplications.InsertOnSubmit(objPostApplication);
            DB.SubmitChanges();

            return objPostApplication;
        }

        public CategoryAppListClass GetCategoryAppUserList(int userId, int categoryId)
        {
            CategoryAppListClass objUserCategoryApps = new CategoryAppListClass();
           objUserCategoryApps.UserApps= (from appMaster in DB.ApplicationMasters
                    join postApp in DB.PostApplications on appMaster.Id equals postApp.AppId
                    where
                    appMaster.CategoryId == categoryId
                    &&
                    postApp.UserId == userId
                    select new ApplicationDto
                    {
                        Id=appMaster.Id,
                        AppId = appMaster.AppId,
                        appAppStoreLink = appMaster.AppStoreLink,
                        AppLink = appMaster.AppLink,
                        AppName = appMaster.AppName
                    }).ToList();
           objUserCategoryApps.CategoryApps = GetCategoryAppsList(categoryId);

           return objUserCategoryApps;

        }

        public List<ApplicationDto> GetCategoryAppsList(int categoryId)
        {
            return (from appMaster in DB.ApplicationMasters
                    where appMaster.CategoryId == categoryId
                    select new ApplicationDto
                    {
                        Id=appMaster.Id,
                        AppId = appMaster.AppId,
                        appAppStoreLink = appMaster.AppStoreLink,
                        AppLink = appMaster.AppLink,
                        AppName = appMaster.AppName
                    }).ToList();
        }


        public void PostScheduleTime(SavePostScheduleTimeDto postSchedule)
        {
            string podatestring = Convert.ToDateTime(postSchedule.ScheduleDate).ToShortDateString();
            Schedule objSchedule = new Schedule();
            var getScheduleDateList = (from usrSchedule in DB.Schedules
                                       where usrSchedule.UserId == postSchedule.UserId
                                       // && Convert.ToDateTime(usrSchedule.DATE).ToShortDateString() == podate.ToShortDateString()
                                       select new SchduleClass
                                       {
                                           Id = usrSchedule.Id,
                                           CreatedDate = usrSchedule.CreatedDate,
                                           DATE = usrSchedule.DATE,
                                           DTime = usrSchedule.DTime,
                                           ModifiedDate = usrSchedule.ModifiedDate,
                                           Session = usrSchedule.Session,
                                           ShortDATE = Convert.ToDateTime(usrSchedule.DATE).ToShortDateString(),
                                           UserId = usrSchedule.UserId
                                       }

                                   ).ToList();
            var getScheduleDate = getScheduleDateList.Where(x => x.ShortDATE == podatestring).FirstOrDefault();

            if (getScheduleDate == null)
            {
                objSchedule.UserId = postSchedule.UserId;
                objSchedule.DTime = postSchedule.DTime;
                objSchedule.DATE = postSchedule.ScheduleDate;
                objSchedule.Session = postSchedule.Session;
                objSchedule.CreatedDate = DateTime.Now;
                DB.Schedules.InsertOnSubmit(objSchedule);
                DB.SubmitChanges();

                ScheduleCategoryTime objSCT = new ScheduleCategoryTime();

                foreach (var itm in postSchedule.CategoryWithTime)
                {
                    objSCT = new ScheduleCategoryTime();
                    objSCT.CategoryId = itm.CategoryId;
                    objSCT.ScheduleId = objSchedule.Id;
                    objSCT.Time = itm.Time;
                    objSCT.CreatedDate = DateTime.Now;
                    DB.ScheduleCategoryTimes.InsertOnSubmit(objSCT);
                    DB.SubmitChanges();

                }

                SelectedApp objSelectedApp = new SelectedApp();
                foreach (var itm in postSchedule.SelectedApps)
                {
                    objSelectedApp = new SelectedApp();
                    objSelectedApp.AppId = itm.AppId;
                    objSelectedApp.ScheduleId = objSchedule.Id;
                    objSelectedApp.CreatedDate = DateTime.Now;
                    DB.SelectedApps.InsertOnSubmit(objSelectedApp);
                    DB.SubmitChanges();

                }
            }
            else
            {
                postSchedule.ScheduleId = getScheduleDate.Id;
                UpdateScheduleTimeManually(postSchedule);

                var getUserSchedules = (from cate in DB.CategoryMasters
                                        join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                                        join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                                        join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                        join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                        where schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
                                        select new UserScheduleDto
                                        {
                                            ScheduleId = scdl.Id,
                                            AppId = apps.Id,
                                            AppLogo = path + apps.AppLink,
                                            AppName = apps.AppName,
                                            CategoryId = cate.Id,
                                            CategoryName = cate.CategoryName,
                                            ScheduleTime = TimeSpan.Parse(scheduleCate.Time).TotalMinutes.ToString(),
                                            ScheduleType = scdl.DTime,
                                            Session = scdl.Session

                                        }
                                        ).Distinct().Where(x => x.ScheduleId == postSchedule.ScheduleId).ToList();

                var getUserImpactFactors = (from impactFact in DB.UserImpactFactors
                                            where impactFact.UserId == postSchedule.UserId
                                            select new ImpactFactorDto
                                            {
                                                UserId = Convert.ToInt32(impactFact.UserId),
                                                Type = impactFact.FactorType,
                                                Duration = impactFact.Duration,
                                                StartTime = impactFact.StartTime

                                            }
                                             ).OrderBy(x => x.StartTime).ToList();



                List<UserScheduleDto> objUserScheduleList = new List<UserScheduleDto>();
                string morningTime = "05:00 AM";
                string eveningTime = "04:00 PM";
                var getUserImpactFactorList = getUserImpactFactors.Where(x => x.StartTime != "" && x.Duration != "").ToList();
                List<object> findBusySchedule = new List<object>();
                foreach (var scheduleList in getUserSchedules)
                {

                    //while (morningTime != "20:00")
                    //{
                    DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
                    var userScheduleTimes = userDateTime.ToString("HH:mm");
                    UserScheduleDto objUserSchedule = new UserScheduleDto();
                    bool busyFind = false;
                    DateTime busyTimeValueInDate = DateTime.Now;
                    string busyFindValue = "";

                    DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                    foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
                    {

                        DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
                        var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

                        DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
                        var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

                        if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
                        {
                            busyFind = true;
                            findBusySchedule.Add(busySchedules);
                            busyTimeValueInDate = busyDateEndTime;
                            busyFindValue = userBusyEndTime;
                            userDateTime = busyDateEndTime;
                            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                            //break;
                        }
                        else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
                        {
                            //break;
                            busyFind = true;
                            findBusySchedule.Add(busySchedules);
                            busyTimeValueInDate = busyDateEndTime;
                            busyFindValue = userBusyEndTime;
                            userDateTime = busyDateEndTime;
                            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                            //break;
                        }
                        else
                        {

                            // break;
                        }

                    }


                    var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
                    if (scheduleList.Session.ToLower() == "morning")
                    {
                        morningTime = userNextScheduleTimes;
                    }
                    else
                    {
                        eveningTime = userNextScheduleTimes;
                    }
                    objUserSchedule.ScheduleId = scheduleList.ScheduleId;
                    objUserSchedule.AppId = scheduleList.AppId;
                    objUserSchedule.AppLogo = path + scheduleList.AppLogo;
                    objUserSchedule.AppName = scheduleList.AppName;
                    objUserSchedule.CategoryId = scheduleList.CategoryId;
                    objUserSchedule.CategoryName = scheduleList.CategoryName;
                    objUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
                    objUserSchedule.ScheduleType = scheduleList.ScheduleType;
                    objUserSchedule.Session = scheduleList.Session;
                    objUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");

                    objUserScheduleList.Add(objUserSchedule);

                    //}

                }

                
                var slotList = (from slot in DB.ScheduleTimeSlots where slot.ScheduleId == postSchedule.ScheduleId select slot
                   ).ToList();

                DB.ScheduleTimeSlots.DeleteAllOnSubmit(slotList);
                DB.SubmitChanges();

                foreach (var tSlot in objUserScheduleList)
                {
                    ScheduleTimeSlot obj = new ScheduleTimeSlot();
                    obj.ScheduleId = tSlot.ScheduleId;
                    obj.AppId = tSlot.AppId;
                    obj.CategoryId = tSlot.CategoryId;
                    obj.ScheduleTime = tSlot.ScheduleTime;
                    obj.EndTime = tSlot.EndTime;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifiedDate = DateTime.Now;
                    DB.ScheduleTimeSlots.InsertOnSubmit(obj);
                    DB.SubmitChanges();

                }
            }
        }



        public UserMaster UpdateCategoryAndProfile(int userId, string profileImg, string oldPwd, string newPwd, CategoryDto2 categories, string dob, string gender, string lat,
            string longt,string name,string username)
        {
            UserMaster um = new UserMaster();
            um = (from user in DB.UserMasters where user.Id == userId select user).SingleOrDefault();
            if (um != null)
            {
                if (!string.IsNullOrEmpty(profileImg))
                    um.ProfileImage = profileImg;
                if (!string.IsNullOrEmpty(newPwd))
                    um.UserPassword = newPwd;

                if (!string.IsNullOrEmpty(dob))
                    um.Dob = dob;
                if (!string.IsNullOrEmpty(gender))
                    um.Gender = gender;
                if (!string.IsNullOrEmpty(lat))
                    um.Latitude = lat;
                if (!string.IsNullOrEmpty(longt))
                    um.Longitude = longt;

                if (!string.IsNullOrEmpty(name))
                    um.Name = name;

                if (!string.IsNullOrEmpty(username))
                    um.UserName = username;
                DB.SubmitChanges();
            }
            if (categories != null)
            {
                var getUserCategory = (from cate in DB.CategoryWithSubCategoryLinks where cate.UserId == userId select cate).ToList();
                DB.CategoryWithSubCategoryLinks.DeleteAllOnSubmit(getUserCategory);
                DB.SubmitChanges();


                CategoryWithSubCategoryLink obj;
                 var dd = (from t in DB.SubCategoryMasters select t).ToList();
            //foreach (var getCate in userCategory.SubCategoryIds)
            //{
               
              
                //foreach (var itm in categories)
                //{
                 foreach (var getCate in categories.SubCategoryId)
                    {
                        var catg = dd.Where(x => x.Id == Convert.ToInt32(getCate)).FirstOrDefault();
                
                        obj = new CategoryWithSubCategoryLink();
                        obj.UserId = userId;
                        if (catg != null)
                {
                    obj.CategoryId = catg.CategoryId;
                }
                        obj.SubCategoryId = getCate;
                        obj.CreatedDate = DateTime.Now;
                        DB.CategoryWithSubCategoryLinks.InsertOnSubmit(obj);
                        DB.SubmitChanges();
                    }
                }
            //}
            return um;
        }

        public bool MatchOldPassword(int userid, string oldPassword)
        {
            var d = (from t in DB.UserMasters where t.Id == userid && t.UserPassword == oldPassword select t).Count();
            if (d == 0)
            {
                return false;
            }
            return true;
        }


        public void SaveGoalsDuringWeek(SaveWeeklyGoalDto objGoalsWeekly)
        {
            SetGoalsDuringWeek obj = new SetGoalsDuringWeek();

           var getGoalsList =(from goals in DB.SetGoalsDuringWeeks where goals.UserId == objGoalsWeekly.UserId select goals).ToList();
           if (getGoalsList.Count > 0)
           {
               DB.SetGoalsDuringWeeks.DeleteAllOnSubmit(getGoalsList);
               DB.SubmitChanges();
           }
            foreach (var itm in objGoalsWeekly.CategoryWithDays)
            {
                obj = new SetGoalsDuringWeek();
                obj.UserId = objGoalsWeekly.UserId;
                obj.Weekly = objGoalsWeekly.Weekly;
                obj.Daily = objGoalsWeekly.Daily;
                obj.CategoryId = Convert.ToInt32(itm.CategoryId);
                obj.CreatedDate = DateTime.Now;
                obj.NoOfDays = itm.NoOfDays;
                DB.SetGoalsDuringWeeks.InsertOnSubmit(obj);
                DB.SubmitChanges();
            }

        }


        public void SaveUserImpactFactors(List<ImpactFactorDto> impactFactors)
        {
            UserImpactFactor obj = new UserImpactFactor();
            int userId = impactFactors.FirstOrDefault().UserId;

            var getImpactFactor = (from impactFact in DB.UserImpactFactors
                                   where impactFact.UserId == userId
                                   select impactFact).ToList();

            if (getImpactFactor.Count > 0)
            {
                DB.UserImpactFactors.DeleteAllOnSubmit(getImpactFactor);
                DB.SubmitChanges();
            }
            foreach (var itm in impactFactors)
            {
                obj = new UserImpactFactor();
                obj.UserId = itm.UserId;
                obj.FactorType = itm.Type;
                obj.StartTime = itm.StartTime;
                obj.Duration = itm.Duration;
                obj.CreatedDate = DateTime.Now;
                DB.UserImpactFactors.InsertOnSubmit(obj);
                DB.SubmitChanges();

            }
        }



        public List<ApplicationDto> EditCategoryApps(int userId, List<SelectedAppsDto> userApps,int categoryId)
        {
            if (categoryId == 0)
            {
                var getSelectedApps = (from cateApps in DB.PostApplications where cateApps.UserId == userId select cateApps).ToList();

                if (getSelectedApps != null)
                {
                    DB.PostApplications.DeleteAllOnSubmit(getSelectedApps);
                    DB.SubmitChanges();
                }
                PostApplication objApps;
                foreach (var itm in userApps)
                {
                    objApps = new PostApplication();
                    objApps.UserId = userId;
                    objApps.AppId = itm.AppId;
                    DB.PostApplications.InsertOnSubmit(objApps);
                    DB.SubmitChanges();
                }
            }
            else
            {
                var getCategoryApps = (from appMaster in DB.ApplicationMasters
                                       join postApp in DB.PostApplications on appMaster.Id equals postApp.AppId
                                       where postApp.UserId == userId && appMaster.CategoryId == categoryId
                                       select postApp).ToList();


                if (getCategoryApps.Count>0)
                {
                    DB.PostApplications.DeleteAllOnSubmit(getCategoryApps);
                    DB.SubmitChanges();
                }
                PostApplication objApps;
                foreach (var itm in userApps)
                {
                    objApps = new PostApplication();
                    objApps.UserId = userId;
                    objApps.AppId = itm.AppId;
                    DB.PostApplications.InsertOnSubmit(objApps);
                    DB.SubmitChanges();
                }
            }

           return (from appMaster in DB.ApplicationMasters
             join postApp in DB.PostApplications on appMaster.Id equals postApp.AppId
             where postApp.UserId == userId
             select new ApplicationDto
             {
                 Id = appMaster.Id,
                 AppId = appMaster.AppId,
                 appAppStoreLink = appMaster.AppStoreLink,
                 AppLink = appMaster.AppLink,
                 AppName = appMaster.AppName
             }).ToList();
        }


        public UserMaster SignInWithApple(string email)
        {
            UserMaster u = new UserMaster();
            u = (from t in DB.UserMasters where t.UserName == email || t.UserEmail==email select t).SingleOrDefault();
            if (u != null)
                return u;
            else
            {
                u = new UserMaster();
                u.RoleId = 2;
                u.LoginType = "1"; //Login with apple
                u.UserName = email;
                u.UserPassword = "";
                u.CreatedDate = DateTime.Now;
                DB.UserMasters.InsertOnSubmit(u);
                DB.SubmitChanges();
                return u;
            }

        }

        public void UpdateScheduleTimeManually(SavePostScheduleTimeDto scheduleTime)
        {
            
            var getScheduleDetails = (from getSchedule in DB.Schedules where getSchedule.Id == scheduleTime.ScheduleId && getSchedule.UserId == scheduleTime.UserId select getSchedule).SingleOrDefault();
            if (getScheduleDetails != null)
            {
                getScheduleDetails.Session = scheduleTime.Session;
                getScheduleDetails.DATE = scheduleTime.ScheduleDate;
                getScheduleDetails.DTime = scheduleTime.DTime;
                getScheduleDetails.UserId = scheduleTime.UserId;
                getScheduleDetails.ModifiedDate = DateTime.Now;
                DB.SubmitChanges();

            }

            var getScheduleCateTime = (from getScheduleCate in DB.ScheduleCategoryTimes where getScheduleCate.ScheduleId == scheduleTime.ScheduleId select getScheduleCate).ToList();

            if (getScheduleCateTime.Count > 0)
            {
                DB.ScheduleCategoryTimes.DeleteAllOnSubmit(getScheduleCateTime);
                DB.SubmitChanges();
            }

            ScheduleCategoryTime objSCT = new ScheduleCategoryTime();

            foreach (var itm in scheduleTime.CategoryWithTime)
            {
                objSCT = new ScheduleCategoryTime();
                objSCT.CategoryId = itm.CategoryId;
                objSCT.ScheduleId = scheduleTime.ScheduleId;
                objSCT.Time = itm.Time;
                objSCT.CreatedDate = DateTime.Now;
                objSCT.ModifiedDate = DateTime.Now;
                DB.ScheduleCategoryTimes.InsertOnSubmit(objSCT);
                DB.SubmitChanges();

            }

            var getScheduleApps = (from scheduleApp in DB.SelectedApps where scheduleApp.ScheduleId == scheduleTime.ScheduleId select scheduleApp).ToList();
            DB.SelectedApps.DeleteAllOnSubmit(getScheduleApps);
            DB.SubmitChanges();

            SelectedApp objSelectedApp = new SelectedApp();
            foreach (var itm in scheduleTime.SelectedApps)
            {
                objSelectedApp = new SelectedApp();
                objSelectedApp.AppId = itm.AppId;
                objSelectedApp.ScheduleId = scheduleTime.ScheduleId;
                objSelectedApp.CreatedDate = DateTime.Now;
                objSelectedApp.ModifiedDate = DateTime.Now;
                DB.SelectedApps.InsertOnSubmit(objSelectedApp);
                DB.SubmitChanges();

            }

        }


        public DashboardDto GetUserDashboardDetails(int userId)
        {
            DashboardDto objDash = new DashboardDto();


            var userSchedule = (from um in DB.UserMasters
                                join getSchedule in DB.Schedules on um.Id equals getSchedule.UserId
                                where getSchedule.UserId == userId
                                select new
                                {
                                    scheduleId = getSchedule.Id,
                                    ScheduleStartTime = getSchedule.DATE,
                                    ProfileImage = path + um.ProfileImage
                                }
                    ).OrderByDescending(x => x.scheduleId).FirstOrDefault();
            int scheduleId = 0;

            if (userSchedule != null)
            {
                scheduleId = userSchedule.scheduleId;
            }

            objDash.ProfileImage = userSchedule != null ? userSchedule.ProfileImage : "";
            objDash.ScheduleStartTime = userSchedule != null ? userSchedule.ScheduleStartTime : "";

           var getTotalDur =GetSpendTimeForCategory(0, userId);
           objDash.Duration = getTotalDur;
            objDash.UserAward = GetAchievementLevel(getTotalDur);

            var getUserSchedules = (from cate in DB.CategoryMasters
                                    join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                                    join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                                    join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                    join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                    where schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
                                    select new UserScheduleDto
                                    {
                                        ScheduleId = scdl.Id,
                                        AppId = apps.Id,
                                        AppLogo = path + apps.AppLink,
                                        AppName = apps.AppName,
                                        CategoryId = cate.Id,
                                        CategoryName = cate.CategoryName,
                                        ScheduleTime = TimeSpan.Parse(scheduleCate.Time).TotalMinutes.ToString(),
                                        ScheduleType = scdl.DTime,
                                        Session = scdl.Session

                                    } 
                                        ).Distinct().Where(x => x.ScheduleId == scheduleId).ToList();

            var getUserImpactFactors = (from impactFact in DB.UserImpactFactors
                                        where impactFact.UserId == userId
                                        select new ImpactFactorDto
                                        {
                                            UserId = Convert.ToInt32(impactFact.UserId),
                                            Type = impactFact.FactorType,
                                            Duration = impactFact.Duration ,
                                            StartTime = impactFact.StartTime 

                                        }
                                         ).OrderBy(x=>x.StartTime).ToList();

            

            List<UserScheduleDto> objUserScheduleList = new List<UserScheduleDto>();
            string morningTime = "05:00 AM";
            string eveningTime = "04:00 PM";
            var getUserImpactFactorList = getUserImpactFactors.Where(x => x.StartTime != "" && x.Duration != "").ToList();
            List<object> findBusySchedule = new List<object>();
            foreach (var scheduleList in getUserSchedules)
            {
                
                //while (morningTime != "20:00")
                //{
                    DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
                    var userScheduleTimes = userDateTime.ToString("HH:mm");
                    UserScheduleDto objUserSchedule = new UserScheduleDto();
                    bool busyFind = false;
                    DateTime busyTimeValueInDate=DateTime.Now;
                    string busyFindValue = "";

                    DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                    foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
                    {

                        DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
                        var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

                        DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
                        var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

                        if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
                        {
                            busyFind = true;
                            findBusySchedule.Add(busySchedules);
                            busyTimeValueInDate = busyDateEndTime;
                            busyFindValue = userBusyEndTime;
                            userDateTime = busyDateEndTime;
                            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                            //break;
                        }
                        else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
                        {
                            //break;
                            busyFind = true;
                            findBusySchedule.Add(busySchedules);
                            busyTimeValueInDate = busyDateEndTime;
                            busyFindValue = userBusyEndTime;
                            userDateTime = busyDateEndTime;
                            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                            //break;
                        }                        
                        else
                        {
                           
                           // break;
                        }

                    }
                    

                    var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
                    if (scheduleList.Session.ToLower() == "morning")
                    {
                        morningTime = userNextScheduleTimes;
                    }
                    else
                    {
                        eveningTime = userNextScheduleTimes;
                    }
                        objUserSchedule.ScheduleId = scheduleList.ScheduleId;
                        objUserSchedule.AppId = scheduleList.AppId;
                        objUserSchedule.AppLogo = path + scheduleList.AppLogo;
                        objUserSchedule.AppName = scheduleList.AppName;
                        objUserSchedule.CategoryId = scheduleList.CategoryId;
                        objUserSchedule.CategoryName = scheduleList.CategoryName;
                        objUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
                        objUserSchedule.ScheduleType = scheduleList.ScheduleType;
                        objUserSchedule.Session = scheduleList.Session;
                        objUserSchedule.SpendCategoryHours = GetSpendTimeForCategory(Convert.ToInt32(scheduleList.CategoryId), userId);
                        objUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");
                    
                        objUserScheduleList.Add(objUserSchedule);

                    //}
               
            }


            var scheduleSlotList = (from slot in DB.ScheduleTimeSlots where slot.ScheduleId == scheduleId select slot
              ).ToList();
            if (scheduleSlotList.Count > 0)
            {
                objDash.UserSchedules = (from cate in DB.CategoryMasters
                                         join ts in DB.ScheduleTimeSlots on cate.Id equals ts.CategoryId
                                         join apps in DB.ApplicationMasters on ts.AppId equals apps.Id
                                         join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                         join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                         where
                                          scdl.UserId == userId
                                          && schApps.ScheduleId == scheduleId && ts.ScheduleId == scheduleId
                                         select new UserScheduleDto
                                         {
                                             ScheduleId = (int)ts.ScheduleId,
                                             AppId = (int)ts.AppId,
                                             AppName = apps.AppName,
                                             AppLogo = path + apps.AppLink,
                                             CategoryId = (int)ts.CategoryId,
                                             CategoryName = cate.CategoryName,
                                             ScheduleTime = ts.ScheduleTime.ToString(),
                                             EndTime = ts.EndTime.ToString(),
                                             ScheduleType = scdl.DTime,
                                             Session = scdl.Session,
                                             SpendCategoryHours = GetSpendTimeForCategory(Convert.ToInt32(ts.CategoryId),userId)
                                         }).OrderBy(x => x.ScheduleTime).ToList();
            }
            else
            {
                objDash.UserSchedules = objUserScheduleList;
            }

            var yesterdaySchedules = (from cate in DB.CategoryMasters
                                      join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                                      join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                                      join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                      join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                      where
                                          //Convert.ToDateTime(scdl.DATE) == Convert.ToDateTime(DateTime.Today.AddDays(-1))
                                        scdl.UserId == userId && schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
                                      select new SchduleClass
                                      {
                                          AppId = apps.Id,
                                          AppLogo = path + apps.AppLink,
                                          AppName = apps.AppName,
                                          CategoryId = cate.Id,
                                          CategoryName = cate.CategoryName,
                                          ScheduleTime = scheduleCate.Time,
                                          ScheduleId = scdl.Id,
                                          ScheduleType = scdl.DTime,
                                          ShortDATE = Convert.ToDateTime(scdl.DATE).ToShortDateString(),
                                          Session = scdl.Session
                                      }
                                      ).Distinct().OrderByDescending(x => x.ScheduleId).ToList();
            var getLastUserSchedule = yesterdaySchedules.Where(x => x.ShortDATE != Convert.ToDateTime(DateTime.Today).ToShortDateString()).FirstOrDefault();
            var getYesterdaySchedules = yesterdaySchedules.Where(x => x.ShortDATE != Convert.ToDateTime(DateTime.Today).ToShortDateString() && x.ScheduleId == getLastUserSchedule.ScheduleId).Select(x => new UserScheduleDto
            {
                AppId = x.AppId,
                AppLogo = path + x.AppLogo,
                AppName = x.AppName,
                CategoryId = x.CategoryId,
                CategoryName = x.CategoryName,
                ScheduleTime = TimeSpan.Parse(x.ScheduleTime).TotalMinutes.ToString(),
                ScheduleId = x.ScheduleId,
                ScheduleType = x.ScheduleType,
                Session = x.Session
            }).Distinct().ToList();

            List<UserScheduleDto> objYestUserScheduleList = new List<UserScheduleDto>();
            morningTime = "05:00 AM";
            eveningTime = "04:00 PM";

            findBusySchedule = new List<object>();
            foreach (var scheduleList in getYesterdaySchedules)
            {


                DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
                var userScheduleTimes = userDateTime.ToString("HH:mm");
                UserScheduleDto objYestUserSchedule = new UserScheduleDto();

                DateTime busyTimeValueInDate = DateTime.Now;
                string busyFindValue = "";

                DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
                {

                    DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
                    var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

                    DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
                    var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

                    if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
                    {

                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
                    {
                        //break;

                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else
                    {
                        // break;
                    }

                }


                var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
                if (scheduleList.Session.ToLower() == "morning")
                {
                    morningTime = userNextScheduleTimes;
                }
                else
                {
                    eveningTime = userNextScheduleTimes;
                }
                objYestUserSchedule.ScheduleId = scheduleList.ScheduleId;
                objYestUserSchedule.AppId = scheduleList.AppId;
                objYestUserSchedule.AppLogo = path + scheduleList.AppLogo;
                objYestUserSchedule.AppName = scheduleList.AppName;
                objYestUserSchedule.CategoryId = scheduleList.CategoryId;
                objYestUserSchedule.CategoryName = scheduleList.CategoryName;
                objYestUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
                objYestUserSchedule.ScheduleType = scheduleList.ScheduleType;
                objYestUserSchedule.Session = scheduleList.Session;
                objYestUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");
                objYestUserScheduleList.Add(objYestUserSchedule);

            }

           int yestScheduleId = getLastUserSchedule == null ? 0 : getLastUserSchedule.ScheduleId;
           var YesterdaySlotList = (from yestSlot in DB.ScheduleTimeSlots where yestSlot.ScheduleId == yestScheduleId select yestSlot
              ).ToList();

            if (YesterdaySlotList.Count > 0)
            {
                objDash.YesterdaySchedules = (from cate in DB.CategoryMasters
                                              join ts in DB.ScheduleTimeSlots on cate.Id equals ts.CategoryId
                                              join apps in DB.ApplicationMasters on ts.AppId equals apps.Id
                                              join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                              join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                              where

                                               scdl.UserId == userId && schApps.ScheduleId == yestScheduleId
                                               && ts.ScheduleId == yestScheduleId
                                              select new UserScheduleDto
                                              {
                                                  ScheduleId = (int)ts.ScheduleId,
                                                  AppId = (int)ts.AppId,
                                                  AppName = apps.AppName,
                                                  AppLogo = path + apps.AppLink,
                                                  CategoryId = (int)ts.CategoryId,
                                                  CategoryName = cate.CategoryName,
                                                  ScheduleTime = ts.ScheduleTime.ToString(),
                                                  EndTime = ts.EndTime.ToString(),
                                                  ScheduleType = scdl.DTime,
                                                  Session = scdl.Session,
                                                  SpendCategoryHours = GetSpendTimeForCategory(Convert.ToInt32(ts.CategoryId), userId)
                                              }).OrderBy(x => x.ScheduleTime).ToList();
            }
            else
            {
                objDash.YesterdaySchedules = objYestUserScheduleList;
            }
            objDash.UserImpactFactors = getUserImpactFactors;
            return objDash;
        }


        public string GetSpendTimeForCategory(int categoryId,int userId)
        {
            string logTime = "";
            var getLogTimeByUser = (from lt in DB.AppsHourForDates
                                    where lt.UserId == userId 
                                    select new
                                    {
                                        categoryId = lt.CategoryId,
                                        spendTime = TimeSpan.Parse(lt.SpendTime).TotalHours
                                    }
                    ).ToList();

            if (categoryId >0)
            {
                logTime = getLogTimeByUser.Where(x=>x.categoryId==categoryId).Sum(x => x.spendTime).ToString();
            }else
            {
                logTime = getLogTimeByUser.Sum(x => x.spendTime ).ToString();
            }

            decimal getHours = decimal.Round(Convert.ToDecimal(logTime));
            logTime = getHours.ToString();
            return logTime;
        }

        public ScheduleDetails GetUserScheduleDetails(int userId)
        {

            ScheduleDetails objUserSchedule = new ScheduleDetails();

            string todayDate = Convert.ToDateTime(DateTime.Today).ToShortDateString();
            string yesterDayDate = Convert.ToDateTime(DateTime.Today.AddDays(-1)).ToShortDateString();

            var todaySchedules = (from cate in DB.CategoryMasters
                                  join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                                  join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                                  join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                  join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                  where
                                      //Convert.ToDateTime(scdl.DATE) == Convert.ToDateTime(DateTime.Today) && 
                                   scdl.UserId == userId && schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
                                  select new SchduleClass
                                  {
                                      AppId = apps.Id,
                                      AppLogo = path + apps.AppLink,
                                      AppName = apps.AppName,
                                      CategoryId = cate.Id,
                                      CategoryName = cate.CategoryName,
                                      ScheduleTime = scheduleCate.Time,
                                      ScheduleId = scdl.Id,
                                      ScheduleType = scdl.DTime,
                                      ShortDATE = Convert.ToDateTime(scdl.DATE).ToShortDateString(),
                                      Session = scdl.Session
                                  }
                                                      ).Distinct().ToList();
            
            var yesterdaySchedules = (from cate in DB.CategoryMasters
                                      join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                                      join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                                      join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                      join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                      where
                                          //Convert.ToDateTime(scdl.DATE) == Convert.ToDateTime(DateTime.Today.AddDays(-1))
                                        scdl.UserId == userId && schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
                                      select new SchduleClass
                                      {
                                          AppId = apps.Id,
                                          AppLogo = path + apps.AppLink,
                                          AppName = apps.AppName,
                                          CategoryId = cate.Id,
                                          CategoryName = cate.CategoryName,
                                          ScheduleTime = scheduleCate.Time,
                                          ScheduleId = scdl.Id,
                                          ScheduleType = scdl.DTime,
                                          ShortDATE = Convert.ToDateTime(scdl.DATE).ToShortDateString(),
                                          Session = scdl.Session
                                      }
                                         ).Distinct().OrderByDescending(x => x.ScheduleId).ToList();
            var getLastUserSchedule = yesterdaySchedules.Where(x => x.ShortDATE != todayDate).FirstOrDefault();
            var fetchTodaySchedules = todaySchedules.Where(x => x.ShortDATE == todayDate).FirstOrDefault();



            var getScheduleDetails = (from getSchedule in DB.Schedules
                                      where getSchedule.UserId == userId
                                      select getSchedule).OrderByDescending(x => x.Id).FirstOrDefault();
            objUserSchedule.ScheduleStartTime = getScheduleDetails.DATE;

            var getTodaySchedules = todaySchedules.Where(x => x.ScheduleId == (fetchTodaySchedules == null ? getLastUserSchedule.ScheduleId : fetchTodaySchedules.ScheduleId)).Select(x => new UserScheduleDto
            {
                AppId = x.AppId,
                AppLogo = path + x.AppLogo,
                AppName = x.AppName,
                CategoryId = x.CategoryId,
                CategoryName = x.CategoryName,
                ScheduleTime = TimeSpan.Parse(x.ScheduleTime).TotalMinutes.ToString(),
                ScheduleId = x.ScheduleId,
                ScheduleType = x.ScheduleType,
                Session = x.Session
            }).Distinct().ToList();
            var getYesterdaySchedules = todaySchedules.Where(x => x.ShortDATE != todayDate && x.ScheduleId == getLastUserSchedule.ScheduleId).Select(x => new UserScheduleDto
            {
                AppId = x.AppId,
                AppLogo = path + x.AppLogo,
                AppName = x.AppName,
                CategoryId = x.CategoryId,
                CategoryName = x.CategoryName,
                ScheduleTime = TimeSpan.Parse(x.ScheduleTime).TotalMinutes.ToString(),
                ScheduleId = x.ScheduleId,
                ScheduleType = x.ScheduleType,
                Session = x.Session
            }).Distinct().ToList();
            var getUserImpactFactors = (from impactFact in DB.UserImpactFactors
                                         where impactFact.UserId == userId
                                         select new ImpactFactorDto
                                         {
                                             UserId = Convert.ToInt32(impactFact.UserId),
                                             Type = impactFact.FactorType,
                                             Duration = impactFact.Duration,
                                             StartTime = impactFact.StartTime

                                         }
                                        ).ToList();


            List<UserScheduleDto> objTodayUserScheduleList = new List<UserScheduleDto>();
            string morningTime = "05:00 AM";
            string eveningTime = "04:00 PM";
            var getUserImpactFactorList = getUserImpactFactors.Where(x => x.StartTime != "" && x.Duration != "").ToList();
            List<object> findBusySchedule = new List<object>();
            foreach (var scheduleList in getTodaySchedules)
            {

                
                DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
                var userScheduleTimes = userDateTime.ToString("HH:mm");
                UserScheduleDto objTodayUserSchedule = new UserScheduleDto();
                
                DateTime busyTimeValueInDate = DateTime.Now;
                string busyFindValue = "";

                DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
                {

                    DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
                    var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

                    DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
                    var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

                    if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
                    {
                        
                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
                    {
                        //break;
                     
                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else
                    {
                        // break;
                    }

                }
                

                var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
                if (scheduleList.Session.ToLower() == "morning")
                {
                    morningTime = userNextScheduleTimes;
                }
                else
                {
                    eveningTime = userNextScheduleTimes;
                }
                objTodayUserSchedule.ScheduleId = scheduleList.ScheduleId;
                objTodayUserSchedule.AppId = scheduleList.AppId;
                objTodayUserSchedule.AppLogo = path + scheduleList.AppLogo;
                objTodayUserSchedule.AppName = scheduleList.AppName;
                objTodayUserSchedule.CategoryId = scheduleList.CategoryId;
                objTodayUserSchedule.CategoryName = scheduleList.CategoryName;
                objTodayUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
                objTodayUserSchedule.ScheduleType = scheduleList.ScheduleType;
                objTodayUserSchedule.Session = scheduleList.Session;
                objTodayUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");
                objTodayUserScheduleList.Add(objTodayUserSchedule);

            }

            List<UserScheduleDto> objYestUserScheduleList = new List<UserScheduleDto>();
             morningTime = "05:00 AM";
             eveningTime = "04:00 PM";
            
             findBusySchedule = new List<object>();
            foreach (var scheduleList in getYesterdaySchedules)
            {


                DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
                var userScheduleTimes = userDateTime.ToString("HH:mm");
                UserScheduleDto objYestUserSchedule = new UserScheduleDto();

                DateTime busyTimeValueInDate = DateTime.Now;
                string busyFindValue = "";

                DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
                {

                    DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
                    var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

                    DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
                    var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

                    if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
                    {
                        
                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
                    {
                        //break;
                        
                        findBusySchedule.Add(busySchedules);
                        busyTimeValueInDate = busyDateEndTime;
                        busyFindValue = userBusyEndTime;
                        userDateTime = busyDateEndTime;
                        getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
                        //break;
                    }
                    else
                    {
                        // break;
                    }

                }


                var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
                if (scheduleList.Session.ToLower() == "morning")
                {
                    morningTime = userNextScheduleTimes;
                }
                else
                {
                    eveningTime = userNextScheduleTimes;
                }
                objYestUserSchedule.ScheduleId = scheduleList.ScheduleId;
                objYestUserSchedule.AppId = scheduleList.AppId;
                objYestUserSchedule.AppLogo = path + scheduleList.AppLogo;
                objYestUserSchedule.AppName = scheduleList.AppName;
                objYestUserSchedule.CategoryId = scheduleList.CategoryId;
                objYestUserSchedule.CategoryName = scheduleList.CategoryName;
                objYestUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
                objYestUserSchedule.ScheduleType = scheduleList.ScheduleType;
                objYestUserSchedule.Session = scheduleList.Session;
                objYestUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");
                objYestUserScheduleList.Add(objYestUserSchedule);

            }
              int getScheduleIdForToday=objTodayUserScheduleList.Count>0? objTodayUserScheduleList.FirstOrDefault().ScheduleId:0;
             var todaySlotList =(from todaySlot in DB.ScheduleTimeSlots where todaySlot.ScheduleId == getScheduleIdForToday select todaySlot
               ).ToList();
             if (todaySlotList.Count > 0)
             {
                 foreach (var tSlot in todaySlotList)
                 {
                     ScheduleTimeSlot obj = new ScheduleTimeSlot();
                     obj.ScheduleId = tSlot.ScheduleId;
                     obj.AppId = tSlot.AppId;
                     obj.CategoryId = tSlot.CategoryId;
                     obj.ScheduleTime = tSlot.ScheduleTime;
                     obj.EndTime = tSlot.EndTime;
                     obj.CreatedDate = DateTime.Now;
                     obj.ModifiedDate = DateTime.Now;
                     DB.ScheduleTimeSlots.InsertOnSubmit(obj);
                     DB.SubmitChanges();

                 }

                 DB.ScheduleTimeSlots.DeleteAllOnSubmit(todaySlotList);
                 DB.SubmitChanges();
             }
             else
             {
                 foreach (var tSlot in objTodayUserScheduleList)
                 {
                     ScheduleTimeSlot obj = new ScheduleTimeSlot();
                     obj.ScheduleId = tSlot.ScheduleId;
                     obj.AppId = tSlot.AppId;
                     obj.CategoryId = tSlot.CategoryId;
                     obj.ScheduleTime = tSlot.ScheduleTime;
                     obj.EndTime = tSlot.EndTime;
                     obj.CreatedDate = DateTime.Now;
                     obj.ModifiedDate = DateTime.Now;
                     DB.ScheduleTimeSlots.InsertOnSubmit(obj);
                     DB.SubmitChanges();

                 }
             }

             int getScheduleIdForYesterday = objYestUserScheduleList.Count > 0 ? objYestUserScheduleList.FirstOrDefault().ScheduleId : 0;
             var YesterdaySlotList = (from yestSlot in DB.ScheduleTimeSlots where yestSlot.ScheduleId == getScheduleIdForYesterday select yestSlot
               ).ToList();
             if (YesterdaySlotList.Count > 0)
             {
                 foreach (var ySlot in YesterdaySlotList)
                 {
                     ScheduleTimeSlot obj = new ScheduleTimeSlot();
                     obj.ScheduleId = ySlot.ScheduleId;
                     obj.AppId = ySlot.AppId;
                     obj.CategoryId = ySlot.CategoryId;
                     obj.ScheduleTime = ySlot.ScheduleTime;
                     obj.EndTime = ySlot.EndTime;
                     obj.CreatedDate = DateTime.Now;
                     obj.ModifiedDate = DateTime.Now;
                     DB.ScheduleTimeSlots.InsertOnSubmit(obj);
                     DB.SubmitChanges();

                 }

                 DB.ScheduleTimeSlots.DeleteAllOnSubmit(YesterdaySlotList);
                 DB.SubmitChanges();
                 
             }
             else
             {
                 foreach (var ySlot in objYestUserScheduleList)
                 {
                     ScheduleTimeSlot obj = new ScheduleTimeSlot();
                     obj.ScheduleId = ySlot.ScheduleId;
                     obj.AppId = ySlot.AppId;
                     obj.CategoryId = ySlot.CategoryId;
                     obj.ScheduleTime = ySlot.ScheduleTime;
                     obj.EndTime = ySlot.EndTime;
                     obj.CreatedDate = DateTime.Now;
                     obj.ModifiedDate = DateTime.Now;
                     DB.ScheduleTimeSlots.InsertOnSubmit(obj);
                     DB.SubmitChanges();

                 }
             }

            
             objUserSchedule.TodaySchedules=(from cate in DB.CategoryMasters
              join ts in DB.ScheduleTimeSlots on cate.Id equals ts.CategoryId
              join apps in DB.ApplicationMasters on ts.AppId equals apps.Id
              join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
              join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
              where

               scdl.UserId == userId && schApps.ScheduleId == getScheduleIdForToday && ts.ScheduleId == getScheduleIdForToday
              select new UserScheduleDto
              {
                  ScheduleId = (int)ts.ScheduleId,
                  AppId =(int) ts.AppId,
                  AppName = apps.AppName,
                  AppLogo = path + apps.AppLink,
                  CategoryId = (int)ts.CategoryId,
                  CategoryName = cate.CategoryName,
                  ScheduleTime = ts.ScheduleTime.ToString(),
                  EndTime = ts.EndTime.ToString(),
                  ScheduleType = scdl.DTime,
                  Session = scdl.Session,
                  SpendCategoryHours = GetSpendTimeForCategory(Convert.ToInt32(ts.CategoryId), userId)
              }).OrderBy(x=>x.ScheduleTime).ToList();


             objUserSchedule.YesterdaySchedules = (from cate in DB.CategoryMasters
                                               join ts in DB.ScheduleTimeSlots on cate.Id equals ts.CategoryId
                                               join apps in DB.ApplicationMasters on ts.AppId equals apps.Id
                                               join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                                               join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                                               where

                                                scdl.UserId == userId && schApps.ScheduleId == getScheduleIdForYesterday
                                                && ts.ScheduleId == getScheduleIdForYesterday
                                               select new UserScheduleDto
                                               {
                                                   ScheduleId = (int)ts.ScheduleId,
                                                   AppId = (int)ts.AppId,
                                                   AppName = apps.AppName,
                                                   AppLogo = path + apps.AppLink,
                                                   CategoryId = (int)ts.CategoryId,
                                                   CategoryName = cate.CategoryName,
                                                   ScheduleTime = ts.ScheduleTime.ToString(),
                                                   EndTime = ts.EndTime.ToString(),
                                                   ScheduleType = scdl.DTime,
                                                   Session = scdl.Session,
                                                   SpendCategoryHours = GetSpendTimeForCategory(Convert.ToInt32(ts.CategoryId), userId)
                                               }).OrderBy(x=>x.ScheduleTime).ToList();

            //objUserSchedule.TodaySchedules = objTodayUserScheduleList;
            //objUserSchedule.YesterdaySchedules = objYestUserScheduleList;
            objUserSchedule.UserImpactFactors = getUserImpactFactors;
            return objUserSchedule;

        }

        public UserAwardDto GetUserAwards(int userId,string date)
        {
            UserAwardDto objAward = new UserAwardDto();


            DateTime now = Convert.ToDateTime(date).Date;
            DateTime today = DateTime.Now;
            var monthSt = new DateTime(now.Year, now.Month, 1);
            var monthEnd = monthSt.AddMonths(1).AddDays(-1);

            if (Convert.ToDateTime(monthEnd).Date >= today.Date && today.Month== now.Month)
            {
                monthEnd = today;
            }
            int noOfWeeks = GetWeeksCovered(monthSt, monthEnd);

            int getLogDaysCount = (from lt in DB.AppsHourForDates
                                   join
                                       cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                   where lt.UserId == userId
                                   && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(monthSt).Date
                                   && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(monthEnd).Date
                                   select Convert.ToDateTime(lt.Date).ToShortDateString()).Distinct().Count();
            objAward.TotalDays = getLogDaysCount;
            var getCategoryGoalsArray =(from user in DB.UserMasters join cate in DB.SetGoalsDuringWeeks on user.Id equals cate.UserId
                    join category in DB.CategoryMasters on cate.CategoryId equals category.Id
              where user.Id == userId && cate.UserId == userId
              select new  {
                  CategoryId=Convert.ToInt32(cate.CategoryId),
                  CategoryName=category.CategoryName,
                  CategoryImage=category.CategoryImage,
                  TotalCategoryGoalHours = (((cate.NoOfDays) * (Convert.ToInt32(cate.Daily)) * (noOfWeeks)))
              }

            ).OrderBy(x=>x.CategoryId).ToList();

            var getCategoryLogHoursList = (from lt in DB.AppsHourForDates
                                           join
                                               cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                           where lt.UserId == userId
                                              && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(monthSt).Date
                                             && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(monthEnd).Date
                                           select new
                                           {
                                               CategoryId = Convert.ToInt32(lt.CategoryId),
                                               CategoryName = cate.CategoryName,
                                               Hours = GetTotalHoursInMin(Convert.ToInt32(lt.CategoryId), userId, Convert.ToDateTime(monthSt).ToString("dd MMMM, yyyy"), Convert.ToDateTime(monthEnd).ToString("dd MMMM, yyyy"))

                                           }).OrderBy(x => x.CategoryId).Distinct().ToList();

            List<AwardCategoryClass> awardCateList=new List<AwardCategoryClass>();
            foreach (var hours in getCategoryLogHoursList)
            {
                var goalAchive = getCategoryGoalsArray.Where(x => x.CategoryId == hours.CategoryId && Convert.ToInt32(hours.Hours) >= Convert.ToInt32(x.TotalCategoryGoalHours)).FirstOrDefault();
                AwardCategoryClass objAwardCate = new AwardCategoryClass();
                objAwardCate.CategoryId = hours.CategoryId;
                objAwardCate.hours = Convert.ToInt32(hours.Hours);
                if(goalAchive !=null)
                {
                   
                    objAwardCate.GoalMeet = "Yes";
                    
                }
                else
                {
                    objAwardCate.GoalMeet = "No";
                }
                objAwardCate.CategoryName = hours.CategoryName;
                objAwardCate.Level = GetAchievementLevel(hours.Hours);
                awardCateList.Add(objAwardCate);
            }
            objAward.caterogyArray = awardCateList;
           var totalMin =GetTotalHoursInMin(0, userId, Convert.ToDateTime(monthSt).ToString("dd MMMM, yyyy"), Convert.ToDateTime(monthEnd).ToString("dd MMMM, yyyy"));
           objAward.Level = GetAchievementLevel(totalMin);
            return objAward;
            
        }



        public string GetAchievementLevel(string hours)
        {
            string achievementLevel = "";
            var getLevelList = (from level in DB.AchievementLevelMasters select new{
                Level=level.Level,
                GoalHours = Convert.ToString(TimeSpan.Parse(level.GoalHours).TotalHours)
            }).ToList();

           string getLastLevelGoal =getLevelList.LastOrDefault().GoalHours;
            foreach (var level in getLevelList)
            {
                if (Convert.ToInt32(hours)==0)
                {
                    achievementLevel = "Level0";
                    return achievementLevel;
                }
                else if (Convert.ToInt32(level.GoalHours) >= Convert.ToInt32(hours))
                {
                    achievementLevel = level.Level;
                    return achievementLevel;
                }
                else if (Convert.ToInt32(hours) >= Convert.ToInt32(getLastLevelGoal))
                {
                    achievementLevel = getLevelList.LastOrDefault().Level;
                    return achievementLevel;
                }
            }

            return achievementLevel;
        }

        public string GetTotalHoursInMin(int categoryId, int userId, string date, string toDate)
        {
            string logTime = "";
            Double totalMin;
            var getLogTimeByUser = (from lt in DB.AppsHourForDates
                                    where lt.UserId == userId && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(date).Date && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(toDate).Date
                                    select new
                                    {
                                        categoryId = lt.CategoryId,
                                        spendTime = Convert.ToDouble(TimeSpan.Parse(lt.SpendTime).TotalHours)
                                    }
                    ).ToList();

            if (categoryId > 0)
            {
                totalMin = getLogTimeByUser.Where(x => x.categoryId == categoryId).Sum(x => x.spendTime);
            }
            else
            {
                totalMin = getLogTimeByUser.Sum(x => x.spendTime);
            }
            decimal getHours = decimal.Round(Convert.ToDecimal(totalMin));
            logTime = getHours.ToString();
            return logTime;
        }
    

        public void SaveSubCategoryDetails(int id, int cateId, string SubCateName)
        {
            SubCategoryMaster obj = new SubCategoryMaster();
            if (id == 0)
            {
                obj.CategoryId = cateId;
                obj.SubCategoryName = SubCateName;
                obj.CreatedDate = DateTime.Now;
                DB.SubCategoryMasters.InsertOnSubmit(obj);
            }
            else
            {
                obj = GetSubCategoryById(id);
                obj.CategoryId = cateId;
                if(!string.IsNullOrEmpty(SubCateName))
                obj.SubCategoryName = SubCateName;
                obj.ModifiedDate = DateTime.Now;
            }

            DB.SubmitChanges();
        }

        public SubCategoryMaster GetSubCategoryById(int subCateId)
        {
            return (from subCate in DB.SubCategoryMasters where subCate.Id == subCateId select subCate).SingleOrDefault();
        }

        public List<SubCategoryList> GetAllSubCategories()
        {
            return (from subCate in DB.SubCategoryMasters
                    join cate in DB.CategoryMasters on subCate.CategoryId equals cate.Id
                    select new SubCategoryList
                    {
                        SubCateId = subCate.Id,
                       // SubCategoryImage = path + subCate.SubCategoryImage,
                        SubCategoryName = subCate.SubCategoryName,
                        CategoryName = cate.CategoryName,
                        CreatedDate = Convert.ToDateTime(subCate.CreatedDate)
                    }).ToList();
        }

        public void DeleteSubCategory(int id)
        {
            var getSubCategory = GetSubCategoryById(id);
            DB.SubCategoryMasters.DeleteOnSubmit(getSubCategory);
            DB.SubmitChanges();
        }

        public List<RoleMaster> GetAllRole()
        {
            return (from role in DB.RoleMasters select role).ToList();
        }

        public List<UserListDto> GetAllUserList()
        {
            return (from user in DB.UserMasters
                    join role in DB.RoleMasters on user.RoleId equals role.Id 
                    select new UserListDto { 
                        UserId=user.Id,
                        RoleName=role.RoleName,
                        UserName=user.UserName,
                        UserPassword=user.UserPassword,
                        Name=user.Name,
                        BirthDay=user.Dob,
                        ProfileImage =user.ProfileImage,
                        CreatedDate=(DateTime)user.CreatedDate,
                        Latitude=user.Latitude,
                        Longitude=user.Longitude,
                        Gender=user.Gender
                    }).ToList();
        }

        public UserMaster GetUserDetailById(int id)
        {
            return (from user in DB.UserMasters where user.Id==id select user).SingleOrDefault();
        }


        public void DeleteUserById(int id)
        {
            var getUsr=GetUserDetailById(id);
            DB.UserMasters.DeleteOnSubmit(getUsr);
            DB.SubmitChanges();
        }

        public ApplicationMaster SaveApplicationDetails(int id,int cateId,string appId,string appName,string appLink,string appStoreLink,List<SubCategoryDto> subCategoryIds)
        {
            ApplicationMaster objAppMaster = new ApplicationMaster();
            if (id == 0)
            {
                objAppMaster.CategoryId = cateId;
                //objAppMaster.AppId = appId;
                objAppMaster.AppId = Guid.NewGuid().ToString();
                objAppMaster.AppName = appName;
                objAppMaster.AppLink = appLink;
                objAppMaster.AppStoreLink = appStoreLink;
                objAppMaster.CreatedDate = DateTime.Now;
                DB.ApplicationMasters.InsertOnSubmit(objAppMaster);
                //send Notification when added new app respect user selected category
                SendNotificationForAddNewApp(cateId);
            }
            else
            {
                objAppMaster = GetApplicationById(id);
                objAppMaster.CategoryId = cateId;
                //objAppMaster.AppId = appId;
                objAppMaster.AppName = appName;
                objAppMaster.AppLink = appLink;
                objAppMaster.AppStoreLink = appStoreLink;
                objAppMaster.ModifiedDate = DateTime.Now;
            }
            DB.SubmitChanges();

            if (subCategoryIds.Count > 0)
            {
                T_ApplicationSubCategory objApplicationSubCategoryLink;
                var getAppSubCategory = (from appSubLink in DB.T_ApplicationSubCategories where appSubLink.AppId == objAppMaster.Id select appSubLink).ToList();
                DB.T_ApplicationSubCategories.DeleteAllOnSubmit(getAppSubCategory);
                DB.SubmitChanges();

                foreach (var itm in subCategoryIds)
                {
                    objApplicationSubCategoryLink = new T_ApplicationSubCategory();
                    objApplicationSubCategoryLink.AppId = objAppMaster.Id;
                    objApplicationSubCategoryLink.SubCategoryId = Convert.ToInt32(itm.SubCategoyId);
                    DB.T_ApplicationSubCategories.InsertOnSubmit(objApplicationSubCategoryLink);
                    DB.SubmitChanges();
                }
            }

            
            return objAppMaster;
        }

        public void SendNotificationForAddNewApp(int cateId)
        {
           var userAppList =(from postApp in DB.PostApplications join app in DB.ApplicationMasters on postApp.AppId equals app.Id 
                             join cate in DB.CategoryMasters on app.CategoryId equals cate.Id
                             where app.CategoryId == cateId select new {
                                 UserId=Convert.ToInt32(postApp.UserId),
                                 CategoryName=cate.CategoryName
                             }).Distinct().ToList();

           foreach (var itm in userAppList)
           {

               var getUser = GetUserDetailById(Convert.ToInt32(itm.UserId));
               var userToken = getUser.DeviceId == null ? "" : getUser.DeviceId;
               if (userToken.Contains(','))
               {
                   var getUserToken = getUser.DeviceId.Split(',');
                   foreach (var token in getUserToken)
                   {
                       SendIhpone(token, "", "New Apps being added to their" + itm.CategoryName + ".");
                   }
               }
               else
               {
                   SendIhpone(getUser.DeviceId, "", "New Apps being added to their" + itm.CategoryName + ".");
               }
              
           }
        }

        public ApplicationMaster GetApplicationById(int id)
        {
            return (from app in DB.ApplicationMasters where app.Id == id select app).SingleOrDefault();
        }

        public void DeleteApplicationById(int id)
        {
            var objAppMaster = GetApplicationById(id);
            DB.ApplicationMasters.DeleteOnSubmit(objAppMaster);
            DB.SubmitChanges();
        }

        public List<ApplicationListDto> GetAllApplication()
        {
            return (from app in DB.ApplicationMasters join cate in DB.CategoryMasters on app.CategoryId equals cate.Id select new ApplicationListDto { 
                Id=app.Id,
                CategoryName=cate.CategoryName,
                AppId=app.AppId,
                AppName=app.AppName,
                AppLink=app.AppLink,
                AppStoreLink=app.AppStoreLink,
                CreatedDate = (DateTime)app.CreatedDate
            }).ToList();
        }


        public List<SubCategoryList> GetSubCategoriesByCategoryId(int categoryId)
        {
            return (from subCate in DB.SubCategoryMasters
                    join cate in DB.CategoryMasters on subCate.CategoryId equals cate.Id
                    where subCate.CategoryId==categoryId  
                    select new SubCategoryList
                    {
                        SubCateId = subCate.Id,
                        // SubCategoryImage = path + subCate.SubCategoryImage,
                        SubCategoryName = subCate.SubCategoryName
                    }).ToList();
        }

        public List<T_ApplicationSubCategory> GetApplicationSubCategoryByAppId(int appId)
        {
            return (from appSubCate in DB.T_ApplicationSubCategories where appSubCate.AppId == appId select appSubCate).ToList();

        }

        public void PostYesterdayScheduleAllApps(int userId)
        {
            
          var getSchedule = GetUserScheduleDetails(userId);

          int scheduleId = getSchedule.TodaySchedules.FirstOrDefault().ScheduleId;
          //int appId = getSchedule.TodaySchedules.FirstOrDefault().AppId;
          //int categoryId = getSchedule.TodaySchedules.FirstOrDefault().CategoryId;
          int scheduleIdYest = getSchedule.YesterdaySchedules.FirstOrDefault().ScheduleId;
          //int appIdYest = getSchedule.YesterdaySchedules.FirstOrDefault().AppId;
          //int categoryIdYest = getSchedule.YesterdaySchedules.FirstOrDefault().CategoryId;    
          var todayScheduleAppList = (from schApps in DB.SelectedApps where schApps.ScheduleId == scheduleId select schApps).ToList();
          var yestScheduleAppList = (from schApps in DB.SelectedApps where  schApps.ScheduleId == scheduleIdYest select schApps).ToList();
          if (todayScheduleAppList.Count > 0)
          {
              DB.SelectedApps.DeleteAllOnSubmit(todayScheduleAppList);
              DB.SubmitChanges();
          }

          var todayScheduleCateList = (from schCate in DB.ScheduleCategoryTimes where  schCate.ScheduleId == scheduleId select schCate).ToList();
          var YestScheduleCateList = (from schCate in DB.ScheduleCategoryTimes where  schCate.ScheduleId == scheduleIdYest select schCate).ToList();
          if (todayScheduleCateList.Count > 0)
          {
              DB.ScheduleCategoryTimes.DeleteAllOnSubmit(todayScheduleCateList);
              DB.SubmitChanges();
          }
           
           foreach (var apps in yestScheduleAppList)
           {
               SelectedApp objApps = new SelectedApp();
               objApps.AppId = apps.AppId;
               objApps.ScheduleId = scheduleId;
               objApps.CreatedDate = DateTime.Now;
               objApps.ModifiedDate = DateTime.Now;
               DB.SelectedApps.InsertOnSubmit(objApps);
               DB.SubmitChanges();

           }

           foreach (var cate in YestScheduleCateList)
           {
               ScheduleCategoryTime objCate = new ScheduleCategoryTime();
               objCate.CategoryId = cate.CategoryId;
               objCate.ScheduleId = scheduleId;
               objCate.Time = cate.Time;
               objCate.CreatedDate = DateTime.Now;
               objCate.ModifiedDate = DateTime.Now;
               DB.ScheduleCategoryTimes.InsertOnSubmit(objCate);
               DB.SubmitChanges();

           }

           //var getUserSchedules = (from cate in DB.CategoryMasters
           //                        join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
           //                        join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
           //                        join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
           //                        join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
           //                        where schApps.ScheduleId == scdl.Id && scheduleCate.ScheduleId == scdl.Id
           //                        select new UserScheduleDto
           //                        {
           //                            ScheduleId = scdl.Id,
           //                            AppId = apps.Id,
           //                            AppLogo = path + apps.AppLink,
           //                            AppName = apps.AppName,
           //                            CategoryId = cate.Id,
           //                            CategoryName = cate.CategoryName,
           //                            ScheduleTime = TimeSpan.Parse(scheduleCate.Time).TotalMinutes.ToString(),
           //                            ScheduleType = scdl.DTime,
           //                            Session = scdl.Session

           //                        }
           //                            ).Distinct().Where(x => x.ScheduleId == scheduleId).ToList();

           //var getUserImpactFactors = (from impactFact in DB.UserImpactFactors
           //                            where impactFact.UserId == userId
           //                            select new ImpactFactorDto
           //                            {
           //                                UserId = Convert.ToInt32(impactFact.UserId),
           //                                Type = impactFact.FactorType,
           //                                Duration = impactFact.Duration,
           //                                StartTime = impactFact.StartTime

           //                            }
           //                             ).OrderBy(x => x.StartTime).ToList();



           //List<UserScheduleDto> objUserScheduleList = new List<UserScheduleDto>();
           //string morningTime = "05:00 AM";
           //string eveningTime = "04:00 PM";
           //var getUserImpactFactorList = getUserImpactFactors.Where(x => x.StartTime != "" && x.Duration != "").ToList();
           //List<object> findBusySchedule = new List<object>();
           //foreach (var scheduleList in getUserSchedules)
           //{

           //    //while (morningTime != "20:00")
           //    //{
           //    DateTime userDateTime = scheduleList.Session.ToLower() == "morning" ? DateTime.Parse(morningTime) : DateTime.Parse(eveningTime);
           //    var userScheduleTimes = userDateTime.ToString("HH:mm");
           //    UserScheduleDto objUserSchedule = new UserScheduleDto();
           //    bool busyFind = false;
           //    DateTime busyTimeValueInDate = DateTime.Now;
           //    string busyFindValue = "";

           //    DateTime getUserDateTime = scheduleList.Session.ToLower() == "morning" ? userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime)) : userDateTime.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
           //    foreach (dynamic busySchedules in getUserImpactFactorList.Except(findBusySchedule))
           //    {

           //        DateTime busyDateStartTime = DateTime.Parse(busySchedules.StartTime == "" ? "00:00" : busySchedules.StartTime);
           //        var userBusyStartTime = busyDateStartTime.ToString("HH:mm");

           //        DateTime busyDateEndTime = DateTime.Parse(busySchedules.Duration == "" ? "00:00" : busySchedules.Duration);
           //        var userBusyEndTime = busyDateEndTime.ToString("HH:mm");

           //        if ((getUserDateTime >= busyDateStartTime.AddMinutes(1) && getUserDateTime <= busyDateEndTime.AddMinutes(1)))
           //        {
           //            busyFind = true;
           //            findBusySchedule.Add(busySchedules);
           //            busyTimeValueInDate = busyDateEndTime;
           //            busyFindValue = userBusyEndTime;
           //            userDateTime = busyDateEndTime;
           //            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
           //            //break;
           //        }
           //        else if ((userDateTime >= busyDateStartTime.AddMinutes(1) && userDateTime <= busyDateEndTime.AddMinutes(1)))
           //        {
           //            //break;
           //            busyFind = true;
           //            findBusySchedule.Add(busySchedules);
           //            busyTimeValueInDate = busyDateEndTime;
           //            busyFindValue = userBusyEndTime;
           //            userDateTime = busyDateEndTime;
           //            getUserDateTime = busyTimeValueInDate.AddMinutes(Convert.ToInt32(scheduleList.ScheduleTime));
           //            //break;
           //        }
           //        else
           //        {

           //            // break;
           //        }

           //    }


           //    var userNextScheduleTimes = getUserDateTime.ToString("HH:mm");
           //    if (scheduleList.Session.ToLower() == "morning")
           //    {
           //        morningTime = userNextScheduleTimes;
           //    }
           //    else
           //    {
           //        eveningTime = userNextScheduleTimes;
           //    }
           //    objUserSchedule.ScheduleId = scheduleList.ScheduleId;
           //    objUserSchedule.AppId = scheduleList.AppId;
           //    objUserSchedule.AppLogo = path + scheduleList.AppLogo;
           //    objUserSchedule.AppName = scheduleList.AppName;
           //    objUserSchedule.CategoryId = scheduleList.CategoryId;
           //    objUserSchedule.CategoryName = scheduleList.CategoryName;
           //    objUserSchedule.ScheduleTime = userDateTime.ToString("HH:mm");
           //    objUserSchedule.ScheduleType = scheduleList.ScheduleType;
           //    objUserSchedule.Session = scheduleList.Session;
           //    objUserSchedule.EndTime = getUserDateTime.ToString("HH:mm");

           //    objUserScheduleList.Add(objUserSchedule);

           //    //}

           //}


           var slotList = (from slot in DB.ScheduleTimeSlots where slot.ScheduleId == scheduleId select slot
              ).ToList();

           DB.ScheduleTimeSlots.DeleteAllOnSubmit(slotList);
           DB.SubmitChanges();

           var yestSlotList = (from slot in DB.ScheduleTimeSlots where slot.ScheduleId == scheduleIdYest select slot
             ).ToList();
           foreach (var tSlot in yestSlotList)
           {
               ScheduleTimeSlot obj = new ScheduleTimeSlot();
               obj.ScheduleId = scheduleId;
               obj.AppId = tSlot.AppId;
               obj.CategoryId = tSlot.CategoryId;
               obj.ScheduleTime = tSlot.ScheduleTime;
               obj.EndTime = tSlot.EndTime;
               obj.CreatedDate = DateTime.Now;
               obj.ModifiedDate = DateTime.Now;
               DB.ScheduleTimeSlots.InsertOnSubmit(obj);
               DB.SubmitChanges();

           }

        }

        public UserScheduleDto PostYesterdayScheduleSingleApps(int scheduleId, int appId)
        {
            string yesterDayDate = Convert.ToDateTime(DateTime.Today.AddDays(-1)).ToShortDateString();
            string todayDate = Convert.ToDateTime(DateTime.Today).ToShortDateString();
            var getAppsList = (from cate in DB.CategoryMasters
                               join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                               join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                               join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                               join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                               where
                                   //Convert.ToDateTime(scdl.DATE) == Convert.ToDateTime(DateTime.Today) && 
                                schApps.AppId == appId
                               select new SchduleClass
                               {
                                   AppId = apps.Id,
                                   AppLogo = path + apps.AppLink,
                                   AppName = apps.AppName,
                                   CategoryId = cate.Id,
                                   CategoryName = cate.CategoryName,
                                   ScheduleTime = scheduleCate.Time,
                                   ScheduleId = scdl.Id,
                                   ScheduleType = scdl.DTime,
                                   ShortDATE = Convert.ToDateTime(scdl.DATE).ToShortDateString(),
                                   Session = scdl.Session
                               }
                                                      ).Distinct().OrderByDescending(x=>x.ScheduleId).ToList();


           var getLastAppDetails =getAppsList.Where(x => x.ShortDATE != todayDate).FirstOrDefault();
            var getApps = getAppsList.Where(x => x.ScheduleId==getLastAppDetails.ScheduleId).FirstOrDefault();

            if (getApps != null)
            {
                SelectedApp objApp = new SelectedApp();
                objApp.ScheduleId = scheduleId;
                objApp.AppId = getApps.AppId;
                objApp.CreatedDate = DateTime.Now;
                objApp.ModifiedDate = DateTime.Now;
                DB.SelectedApps.InsertOnSubmit(objApp);
                DB.SubmitChanges();


                var getYestScheduleCate = (from scheduleCate in DB.ScheduleCategoryTimes where scheduleCate.ScheduleId == getApps.ScheduleId && scheduleCate.CategoryId == getApps.CategoryId select scheduleCate).FirstOrDefault();
                if (getYestScheduleCate != null)
                {

                    ScheduleCategoryTime objCate = new ScheduleCategoryTime();
                    objCate.CategoryId = getYestScheduleCate.CategoryId;
                    objCate.ScheduleId = scheduleId;
                    objCate.Time = getYestScheduleCate.Time;
                    objCate.CreatedDate = DateTime.Now;
                    objCate.ModifiedDate = DateTime.Now;
                    DB.ScheduleCategoryTimes.InsertOnSubmit(objCate);
                    DB.SubmitChanges();

                }

                return (from cate in DB.CategoryMasters
                        join scheduleCate in DB.ScheduleCategoryTimes on cate.Id equals scheduleCate.CategoryId
                        join apps in DB.ApplicationMasters on scheduleCate.CategoryId equals apps.CategoryId
                        join schApps in DB.SelectedApps on apps.Id equals schApps.AppId
                        join scdl in DB.Schedules on schApps.ScheduleId equals scdl.Id
                        where schApps.ScheduleId == scheduleId && schApps.AppId == getApps.AppId
                        select new UserScheduleDto
                        {
                            ScheduleId = scdl.Id,
                            AppId = apps.Id,
                            AppLogo = path + apps.AppLink,
                            AppName = apps.AppName,
                            CategoryId = cate.Id,
                            CategoryName = cate.CategoryName,
                            ScheduleTime = scheduleCate.Time,
                            ScheduleType = scdl.DTime,
                            Session = scdl.Session

                        }
                                              ).FirstOrDefault();

            }

            return null;
        }

        public void EditScheduleDetails(int scheduleId,string scheduleTime,int appId)
        {
           
            var getApps = (from app in DB.ApplicationMasters where app.Id == appId select app).FirstOrDefault();
            
            if (getApps != null)
            {
               int categoryId=(int)getApps.CategoryId;
               var getScheduleCate = (from scheduleCate in DB.ScheduleCategoryTimes where scheduleCate.ScheduleId == scheduleId && scheduleCate.CategoryId == categoryId select scheduleCate).FirstOrDefault();
               var getScheduleSlots = (from slots in DB.ScheduleTimeSlots where slots.ScheduleId == scheduleId && slots.AppId==appId && slots.CategoryId == categoryId select slots).FirstOrDefault();
               if (getScheduleCate != null && getScheduleSlots !=null)
                {
                    getScheduleSlots.ScheduleTime = scheduleTime;
                    double timeInMin = TimeSpan.Parse(getScheduleCate.Time).TotalMinutes;
                    DateTime secheduleEndTimeInDate = DateTime.Parse(scheduleTime).AddMinutes(Convert.ToInt32(timeInMin));
                    getScheduleSlots.EndTime = secheduleEndTimeInDate.ToString("HH:mm");
                    DB.SubmitChanges();
                }
            }
        }

        public void SaveAppsHourForDate(EnterAppsHoursClass appHours)
        {
            AppsHourForDate obj = new AppsHourForDate();
            obj.AppId = appHours.AppId;
            obj.UserId = appHours.UserId;
            obj.CategoryId = appHours.CategoryId;
            obj.SpendTime = appHours.SpendTime;
            obj.Date = appHours.Date;
            obj.CreatedDate = DateTime.Now;
            DB.AppsHourForDates.InsertOnSubmit(obj);
            DB.SubmitChanges();
        }

        public EmptyHourListClass GetEmptyHoursDateForApps(int userId, int appId, int categoryId)
        {
            EmptyHourListClass getEmptyDateForHours=new EmptyHourListClass();

           var getScheduleDetails =(from schedule in DB.Schedules
             join apps in DB.SelectedApps on schedule.Id equals apps.ScheduleId
             join cate in DB.ScheduleCategoryTimes on schedule.Id equals cate.ScheduleId
             where schedule.UserId == userId && apps.AppId == appId && cate.CategoryId == categoryId
                                    select schedule)
                                    //.OrderByDescending(x => x.Id)
                                    .FirstOrDefault();
            //List<DateTime> dateList = new List<DateTime>();
            List<ScheduleDateClass> scheduleDateList = new List<ScheduleDateClass>();
           if (getScheduleDetails != null)
           {
               DateTime StartDate = Convert.ToDateTime(getScheduleDetails.DATE).Date;
               DateTime EndDate = DateTime.Now.Date;
              
               DateTime currentDate = StartDate;

               ScheduleDateClass obj = new ScheduleDateClass();

               obj.Date = currentDate.Date.ToShortDateString();

               scheduleDateList.Add(obj);
               while (currentDate < EndDate)
               {
                    obj = new ScheduleDateClass();
                   //dateList.Add(currentDate);
                   currentDate = currentDate.AddDays(1);
                   obj.Date = currentDate.Date.ToShortDateString();

                   scheduleDateList.Add(obj);
               }

           }
           
           var getLogScheduleAppDate = (from appsHours in DB.AppsHourForDates
                                                   where appsHours.UserId == userId && appsHours.CategoryId==categoryId && appsHours.AppId==appId
                                        select new ScheduleDateClass
                                                   {
                                                      Date =Convert.ToDateTime(appsHours.Date).Date.ToShortDateString()

                                                   }).Distinct().ToList();

          

          List<string> dateList = new List<string>();
          foreach (var itm in scheduleDateList)
          {
              if (!getLogScheduleAppDate.Exists(x => x.Date == itm.Date))
              {
                  dateList.Add(Convert.ToDateTime(itm.Date).ToString("dd MMMM, yyyy"));
              }
          }

          

          getEmptyDateForHours.EmptyDateArray = dateList;


            //getEmptyDateForHours.EmptyDateArray = (from schedule in DB.Schedules
            //        join apps in DB.SelectedApps on schedule.Id equals apps.ScheduleId
            //        join cate in DB.ScheduleCategoryTimes on schedule.Id equals cate.ScheduleId
            //        join appsHours in DB.AppsHourForDates on Convert.ToDateTime(schedule.DATE).Date equals Convert.ToDateTime(appsHours.Date).Date
            //        into tempAppsHours
            //        from hours in tempAppsHours.DefaultIfEmpty()
            //        where schedule.UserId == userId && apps.AppId==appId && cate.CategoryId==categoryId && hours.Date==null
            //                                       select schedule.DATE).Distinct().ToList();
         
            return getEmptyDateForHours;
        }

       

        public void SaveLastActiveAppDate(int userId, string date)
        {
            var getUser = (from usr in DB.UserMasters where usr.Id==userId select usr).SingleOrDefault();
            
            if (getUser != null)
            {
                getUser.LastActiveAppDate = date;
                DB.SubmitChanges();
            }
        }

        public bool CheckExistScheduleTime(int scheduleId, string scheduleTime,int appId)
        {
            var getSchduleSlotList = (from slot in DB.ScheduleTimeSlots where slot.ScheduleId == scheduleId && slot.AppId != appId select slot).ToList();

            
            var getApps = (from app in DB.ApplicationMasters where app.Id == appId select app).FirstOrDefault();

            DateTime secheduleEndTimeInDate = DateTime.Now;
            if (getApps != null)
            {
                int categoryId = (int)getApps.CategoryId;
                var getScheduleCate = (from scheduleCate in DB.ScheduleCategoryTimes where scheduleCate.ScheduleId == scheduleId && scheduleCate.CategoryId == categoryId select scheduleCate).FirstOrDefault();
                double timeInMin = TimeSpan.Parse(getScheduleCate.Time).TotalMinutes;
                secheduleEndTimeInDate = DateTime.Parse(scheduleTime).AddMinutes(Convert.ToInt32(timeInMin));
            }

            foreach (var slot in getSchduleSlotList)
            {
                //if ((DateTime.Parse(scheduleTime).AddMinutes(1) >= DateTime.Parse(slot.ScheduleTime) && DateTime.Parse(scheduleTime).AddMinutes(1) <= DateTime.Parse(slot.EndTime)))
                //{
                //    return true;
                //}

                if ((secheduleEndTimeInDate >= DateTime.Parse(slot.ScheduleTime).AddMinutes(1) && secheduleEndTimeInDate <= DateTime.Parse(slot.EndTime))
                    ||
                    (DateTime.Parse(scheduleTime).AddMinutes(1) >= DateTime.Parse(slot.ScheduleTime) && DateTime.Parse(scheduleTime).AddMinutes(1) <= DateTime.Parse(slot.EndTime))
                    )
                {
                    return true;
                }

            }

            var getSchduleUser = (from scheduleUser in DB.Schedules where scheduleUser.Id == scheduleId select scheduleUser).SingleOrDefault();

            if (getSchduleUser != null)
            {
                var getbusyTimesList = (from busyTimes in DB.UserImpactFactors where busyTimes.UserId == getSchduleUser.UserId && busyTimes.StartTime !="" && busyTimes.Duration!="" select busyTimes).ToList();

                foreach (var slot in getbusyTimesList)
                {
                    //if ((DateTime.Parse(scheduleTime).AddMinutes(1) >= DateTime.Parse(slot.StartTime) && DateTime.Parse(scheduleTime).AddMinutes(1) <= DateTime.Parse(slot.Duration)))
                    //{
                    //    return true;
                    //}

                    if ((secheduleEndTimeInDate >= DateTime.Parse(slot.StartTime).AddMinutes(1) && secheduleEndTimeInDate <= DateTime.Parse(slot.Duration)) 
                        ||
                        (DateTime.Parse(scheduleTime).AddMinutes(1) >= DateTime.Parse(slot.StartTime) && DateTime.Parse(scheduleTime).AddMinutes(1) <= DateTime.Parse(slot.Duration))
                        )
                    {
                        return true;
                    }
                    

                }
            }

            return false;
        }

        public List<UserStaticsClass> GetUserStaticsData(int userId, string date, string option)
        {
            List<UserStaticsClass> userStaticList = new List<UserStaticsClass>();
            UserStaticsClass objStaticsData;
            
            if (option.ToLower() == "week")
            {
                DateTime startDate = Convert.ToDateTime(date).Date;
                DateTime endDate = Convert.ToDateTime(date).AddDays(7).Date;

                while (startDate < endDate)
                {
                    objStaticsData = new UserStaticsClass();
                    objStaticsData.Date = Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy");
                    objStaticsData.Day = Convert.ToDateTime(startDate).DayOfWeek.ToString();
                    objStaticsData.TotalHour = GetLogTimeForUserStatics(0, userId, Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy"), Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy"));
                    objStaticsData.CategoryArray = (from lt in DB.AppsHourForDates
                                                    join
                                                        cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                                    where lt.UserId == userId
                                                    && Convert.ToDateTime(lt.Date).Date == Convert.ToDateTime(startDate).Date
                                                    select new UserCategoryStaticsClass
                                                    {
                                                        CategoryId = Convert.ToInt32(lt.CategoryId),
                                                        CategoryName = cate.CategoryName,
                                                        Hours = GetLogTimeForUserStatics(cate.Id, userId, Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy"), Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy")),
                                                        SelectedCategoryAppsArray = GetCategoryAppsArray(Convert.ToInt32(lt.CategoryId), userId, date)
                                                    }

                              ).Distinct().ToList();
                    startDate = Convert.ToDateTime(startDate).AddDays(1).Date;
                    userStaticList.Add(objStaticsData);
                }
            }
            else if (option.ToLower() == "month")
            {
                DateTime now = Convert.ToDateTime(date).Date;
                DateTime today = DateTime.Now;
                var monthSt = new DateTime(now.Year, now.Month, 1);
                var monthEnd = monthSt.AddMonths(1).AddDays(-1);

                if (Convert.ToDateTime(monthEnd).Date >= today.Date && today.Month== now.Month)
                {
                    monthEnd = today;
                }
                int noOfWeeks = GetWeeksCovered(monthSt, monthEnd);
                
               int getLogDaysCount =(from lt in DB.AppsHourForDates
                 join
                     cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                 where lt.UserId == userId
                 && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(monthSt).Date
                 && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(monthEnd).Date
                 select Convert.ToDateTime(lt.Date).ToShortDateString()).Distinct().Count();
                //int days=monthEnd.Day;
               
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
             
                objStaticsData = new UserStaticsClass();
                objStaticsData.Month = String.Format("{0:MMMM}", Convert.ToDateTime(date));
                objStaticsData.TotalHour = GetLogTimeForUserStatics(0, userId, Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy"), Convert.ToDateTime(endDate).ToString("dd MMMM, yyyy"));
                //objStaticsData.AvgDaySpentWeekly = Math.Round(Convert.ToDouble(objStaticsData.TotalHour) / noOfWeeks).ToString();
                objStaticsData.AvgDaySpentWeekly = Convert.ToInt32(getLogDaysCount/noOfWeeks).ToString();
                objStaticsData.AvgTimeSpentDaily =(Convert.ToDouble(objStaticsData.TotalHour) / getLogDaysCount).ToString();
                objStaticsData.CategoryArray = (from lt in DB.AppsHourForDates
                                                join
                                                    cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                                where lt.UserId == userId
                                                && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(startDate).Date  
                                                && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(endDate).Date
                                                select new UserCategoryStaticsClass
                                                {
                                                    CategoryId = Convert.ToInt32(lt.CategoryId),
                                                    CategoryName = cate.CategoryName,
                                                    Hours = GetLogTimeForUserStatics(cate.Id, userId, Convert.ToDateTime(startDate).ToString("dd MMMM, yyyy"), Convert.ToDateTime(endDate).ToString("dd MMMM, yyyy")),
                                                    SelectedCategoryAppsArray = GetCategoryAppsArray(Convert.ToInt32(lt.CategoryId), userId, date)
                                                }

                          ).Distinct().ToList();

                
               var getTopActivity =objStaticsData.CategoryArray.FirstOrDefault(x => x.Hours == objStaticsData.CategoryArray.Max(h => h.Hours));
               if (getTopActivity != null)
               {
                   int getLogDaysForTopActivity = (from lt in DB.AppsHourForDates
                                          join
                                              cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                          where lt.UserId == userId && lt.CategoryId==getTopActivity.CategoryId
                                          && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(monthSt).Date
                                          && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(monthEnd).Date
                                          select Convert.ToDateTime(lt.Date).ToShortDateString()).Distinct().Count();
                   
                   objStaticsData.TopActivityName = getTopActivity.CategoryName;
                   objStaticsData.TopActivityHourMonthly = getTopActivity.Hours;
                   objStaticsData.AvgTopActivityDaySpentWeekly = (Convert.ToDouble(getLogDaysForTopActivity) / noOfWeeks).ToString();
                   objStaticsData.AvgTopActivitySpentDaily = (Convert.ToDouble(getTopActivity.Hours) / getLogDaysForTopActivity).ToString();
                  
               }

                userStaticList.Add(objStaticsData);
            }
            else
            {
                objStaticsData = new UserStaticsClass();
                objStaticsData.Date = Convert.ToDateTime(date).ToString("dd MMMM, yyyy"); 
                objStaticsData.Day = Convert.ToDateTime(date).DayOfWeek.ToString();

                objStaticsData.TotalHour = GetLogTimeForUserStatics(0, userId, date,date);
                objStaticsData.CategoryArray = (from lt in DB.AppsHourForDates
                                                join
                                                    cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                                where lt.UserId == userId
                                                && Convert.ToDateTime(lt.Date).Date == Convert.ToDateTime(date).Date
                                                select new UserCategoryStaticsClass
                                                {
                                                    CategoryId = Convert.ToInt32(lt.CategoryId),
                                                    CategoryName = cate.CategoryName,
                                                    Hours = GetLogTimeForUserStatics(cate.Id, userId, date,date),
                                                    SelectedCategoryAppsArray = GetCategoryAppsArray(Convert.ToInt32(lt.CategoryId), userId, date)
                                                }

                          ).Distinct().ToList();
                userStaticList.Add(objStaticsData);
            }
            return userStaticList;
        }


        public static int Weeks(int year, int month)
        {
            DayOfWeek wkstart = DayOfWeek.Monday;

            DateTime first = new DateTime(year, month, 1);
            int firstwkday = (int)first.DayOfWeek;
            int otherwkday = (int)wkstart;

            int offset = ((otherwkday + 7) - firstwkday) % 7;

            double weeks = (double)(DateTime.DaysInMonth(year, month) - offset) / 7d;

            return (int)Math.Ceiling(weeks);
        }

       public static int GetWeeksCovered(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
                throw new ArgumentException("endDate cannot be less than startDate");

            return (GetBeginningOfWeek(endDate).Subtract(GetBeginningOfWeek(startDate)).Days / 7) + 1;
        }

       public static DateTime GetBeginningOfWeek(DateTime date)
        {
            return date.AddDays(-1 * (int)date.DayOfWeek).Date;
        }

        public List<string> GetCategoryAppsArray(int categoryId, int userId,string date)
        {
           var categoryAppsList =(from lt in DB.AppsHourForDates
             join apps in DB.ApplicationMasters on lt.AppId equals apps.Id
             where lt.UserId == userId
             && Convert.ToDateTime(lt.Date).Date == Convert.ToDateTime(date).Date && lt.CategoryId == categoryId
             select apps).ToList();

           List<string> appList = new List<string>();
           foreach (var itm in categoryAppsList)
           {
               appList.Add(itm.AppName);
           }

           return appList;
        }

        public string GetLogTimeForUserStatics(int categoryId, int userId,string date,string toDate)
        {
            string logTime = "";
            Double totalMin;
            var getLogTimeByUser = (from lt in DB.AppsHourForDates
                                    where lt.UserId == userId && Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(date).Date && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(toDate).Date
                                    select new
                                    {
                                        categoryId = lt.CategoryId,
                                        spendTime = TimeSpan.Parse(lt.SpendTime).TotalMinutes
                                    }
                    ).ToList();

            if (categoryId > 0)
            {
                totalMin = getLogTimeByUser.Where(x => x.categoryId == categoryId).Sum(x => x.spendTime);
            }
            else
            {
                totalMin = getLogTimeByUser.Sum(x => x.spendTime);
            }

            var timeSpan = TimeSpan.FromMinutes(totalMin);
            //int hh = timeSpan.Hours;
            //int mm = timeSpan.Minutes;
            //int ss = timeSpan.Seconds;
            //logTime = hh + "." + mm;
            logTime = string.Format("{0:00}.{1:00}", (int)timeSpan.TotalHours, timeSpan.Minutes);
            return logTime;
        }

        //public void SendTestPush()
        //{
        //    int userid = 94;
        //    SendIhpone("26bbacdf8b223d5cb2f86f1de7d44424be354b8e185d8944b9a615df717711b1", "\"FromUserId\":\"" + userid.ToString() + "\"", "Vinay" + " " + "has invite you to join this apps");
        //}
        public void NotificationSucceeded(ApnsNotification notification)
        {
            //Do something here
            //WriteLog("pass");
            //var path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "Log.txt";
          // var path =System.Web.HttpContext.Current.Server.MapPath("Log.txt");
           
            using (StreamWriter w = File.AppendText(logPath))
            {
                Log(notification.Payload.ToString(), w,"Success",notification.DeviceToken);
                
            }
        }

        public static void Log(string logMessage, TextWriter w,string status,string token)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine(DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("Token  :" + token);
            w.WriteLine(status + "  :" + logMessage);
            w.WriteLine("-------------------------------");
        }
        public void SendIhpone(string token, string otherparam, string message)
        {
            // Configuration (NOTE: .pfx can also be used here)
            var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox,
              System.Web.HttpContext.Current.Server.MapPath("uTimePushDev.p12"), "1234");
            //config.ValidateServerCertificate = false;
            // Create a new broker
            var apnsBroker = new ApnsServiceBroker(config);

            // Wire up events
            apnsBroker.OnNotificationFailed += NotificationFailed;

            apnsBroker.OnNotificationSucceeded += NotificationSucceeded;

            // Start the broker
            apnsBroker.Start();


            // Queue a notification to send
            apnsBroker.QueueNotification(new ApnsNotification
            {
                DeviceToken = token,
                Payload = JObject.Parse("{\"aps\":{\"alert\" :  \"" + message + "\" ,  \"badge\":7 , \"sound\":\"default\" , \"content-available\" : 1}, " + otherparam + " }"),

            });

        }


        public void NotificationFailed(ApnsNotification notification, AggregateException aggregateEx)
        {
            //Do something here
            //  WriteLog("pass");
            aggregateEx.Handle(ex =>
            {

                // See what kind of exception it was to further diagnose
                if (ex is ApnsNotificationException)
                {
                    var notificationException = (ApnsNotificationException)ex;

                    // Deal with the failed notification
                    var apnsNotification = notificationException.Notification;
                    var statusCode = notificationException.ErrorStatusCode;

                    using (StreamWriter w = File.AppendText(logPath))
                    {
                        Log(statusCode.ToString(), w, "Failed",notification.DeviceToken);

                    }

                }
                else
                {
                    // Inner exception might hold more useful information like an ApnsConnectionException           
                    // Console.WriteLine ($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                }

                // Mark it as handled
                return true;
            });


        }

        public void SendNotificationForLast7DaysInActive()
        {
            var getInActiveUserList = (from user in DB.UserMasters where  user.LastActiveAppDate != null && user.DeviceId !=null && user.DeviceId !="" select new {
                userId=user.Id,
                userName=user.Name,
               date =UnixTimeToDateTime(user.LastActiveAppDate).Date,
               deviceToken=user.DeviceId
            }).ToList();

           var getInActiveUsers =getInActiveUserList.Where(x => x.date <= DateTime.Now.AddDays(-7).Date).ToList();
           if (getInActiveUserList.Count > 0)
           {
               foreach (var inActiveUser in getInActiveUsers)
               {
                    var userToken =inActiveUser.deviceToken==null?"":inActiveUser.deviceToken;
                    if (userToken.Contains(','))
                    {
                        var inActiveUserToken = inActiveUser.deviceToken.Split(',');
                        foreach (var token in inActiveUserToken)
                        {
                            SendIhpone(token, "", "Hii, " + inActiveUser.userName + " " + "You are not active from Last 7 Days.");
                        }
                    }
                    else
                    {
                        SendIhpone(inActiveUser.deviceToken, "", "Hii, " + inActiveUser.userName + " " + "You are not active from Last 7 Days.");
                    }
               }
           }
        }

        public DateTime UnixTimeToDateTime(string unixtime)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long ut = (long)Convert.ToInt64(unixtime);
            dtDateTime = dtDateTime.AddMilliseconds(ut).ToLocalTime();
            return dtDateTime;
        }


        public void SendNotificationForUpcomingSchedule()
        {
            DateTime currentTime = Convert.ToDateTime(DateTime.Now.ToShortTimeString()).AddHours(5).AddMinutes(30);
            //DateTime scheduleTime = DateTime.Parse("17:30").AddMinutes(-5);
            var getScheduleList = (from schedule in DB.Schedules
                                   select new UserScheduleClass
                                       {
                                           ScheduleId=schedule.Id,
                                           UserId = Convert.ToInt32(schedule.UserId),
                                           ScheduleStartDate = Convert.ToDateTime(schedule.DATE).ToShortDateString()
                                       }).OrderByDescending(x => x.ScheduleId).ToList();

            List<UserScheduleClass> userScheduleList = new List<UserScheduleClass>();
            foreach (var sdl in getScheduleList)
            {
                UserScheduleClass objSchedule = new UserScheduleClass();
                if (userScheduleList.Exists(e => e.UserId == sdl.UserId) == false)
                {
                    var getScheduleData = getScheduleList.Where(x => x.UserId == sdl.UserId).OrderByDescending(y => y.ScheduleId).FirstOrDefault();
                    objSchedule.ScheduleId = getScheduleData.ScheduleId;
                    objSchedule.UserId = getScheduleData.UserId;
                    objSchedule.ScheduleStartDate = getScheduleData.ScheduleStartDate;
                    userScheduleList.Add(objSchedule);
                }
               
            }

            if (userScheduleList.Count > 0)
            {
              
                foreach (var sdl in userScheduleList)
                {
                    var getUpcomingScheduleSlot = (from scheduleSlot in DB.ScheduleTimeSlots
                                                   join app in DB.ApplicationMasters on scheduleSlot.AppId equals app.Id
                                                   join cate in DB.CategoryMasters on scheduleSlot.CategoryId equals cate.Id
                                                   where scheduleSlot.ScheduleId == sdl.ScheduleId 
                                                   
                                                   select new
                                                   { 
                                                       UserId=sdl.UserId,
                                                       AppId=scheduleSlot.AppId,
                                                       CategoryId=scheduleSlot.CategoryId,
                                                       AppName=app.AppName,
                                                       ScheduleTime=DateTime.Parse(scheduleSlot.ScheduleTime),
                                                       ScheduleTimeBefore5Min = DateTime.Parse(scheduleSlot.ScheduleTime).AddMinutes(-5),
                                                       EndTime = DateTime.Parse(scheduleSlot.EndTime)
                                                   }).ToList();

                    if (getUpcomingScheduleSlot.Count>0)
                    {
                        //var upcomingSlot = getUpcomingScheduleSlot.Where(x => currentTime >= x.ScheduleTimeBefore5Min && currentTime <= x.ScheduleTime).FirstOrDefault();
                        var upcomingSlot = getUpcomingScheduleSlot.Where(x => x.ScheduleTimeBefore5Min==currentTime).FirstOrDefault();
                        if (upcomingSlot != null)
                        {
                            var getUser = GetUserDetailById(Convert.ToInt32(upcomingSlot.UserId));
                            if (!string.IsNullOrEmpty(getUser.DeviceId)) {
                                var userToken = getUser.DeviceId == null ? "" : getUser.DeviceId;
                                if (userToken.Contains(','))
                                {
                                    var getUserToken = getUser.DeviceId.Split(',');
                                    foreach (var token in getUserToken)
                                    {
                                        SendIhpone(token, "\"Type\":\"" + "Upcoming" + "\",\"AppName\":\"" + upcomingSlot.AppName + "\"", "Your upcoming schedule for " + upcomingSlot.AppName + " is starting in 5 minute.");
                                    }
                                }
                                else
                                {
                                    SendIhpone(getUser.DeviceId, "\"Type\":\"" + "Upcoming" + "\",\"AppName\":\"" + upcomingSlot.AppName + "\"", "Your upcoming schedule for " + upcomingSlot.AppName + " is starting in 5 minute.");
                                }
                           
                            }
                            
                        }
                       
                       
                    }
                }
                                      
            }

        }

        public void SendNotificationForScheduleEndTime()
        {
            DateTime currentTime = Convert.ToDateTime(DateTime.Now.ToShortTimeString()).AddHours(5).AddMinutes(30);
            //DateTime scheduleTime = DateTime.Parse("17:30").AddMinutes(-5);
            var getScheduleList = (from schedule in DB.Schedules
                                   select new UserScheduleClass
                                   {
                                       ScheduleId = schedule.Id,
                                       UserId = Convert.ToInt32(schedule.UserId),
                                       ScheduleStartDate = Convert.ToDateTime(schedule.DATE).ToShortDateString()
                                   }).OrderByDescending(x => x.ScheduleId).ToList();

            List<UserScheduleClass> userScheduleList = new List<UserScheduleClass>();
            foreach (var sdl in getScheduleList)
            {
                UserScheduleClass objSchedule = new UserScheduleClass();
                if (userScheduleList.Exists(e => e.UserId == sdl.UserId) == false)
                {
                    var getScheduleData = getScheduleList.Where(x => x.UserId == sdl.UserId).OrderByDescending(y => y.ScheduleId).FirstOrDefault();
                    objSchedule.ScheduleId = getScheduleData.ScheduleId;
                    objSchedule.UserId = getScheduleData.UserId;
                    objSchedule.ScheduleStartDate = getScheduleData.ScheduleStartDate;
                    userScheduleList.Add(objSchedule);
                }

            }

            if (userScheduleList.Count > 0)
            {

                foreach (var sdl in userScheduleList)
                {
                    var getScheduleSlot = (from scheduleSlot in DB.ScheduleTimeSlots
                                                   join app in DB.ApplicationMasters on scheduleSlot.AppId equals app.Id
                                                   join cate in DB.CategoryMasters on scheduleSlot.CategoryId equals cate.Id
                                                   where scheduleSlot.ScheduleId == sdl.ScheduleId

                                                   select new
                                                   {
                                                       UserId = sdl.UserId,
                                                       AppId = scheduleSlot.AppId,
                                                       CategoryId = scheduleSlot.CategoryId,
                                                       AppName = app.AppName,
                                                       CategoryName=cate.CategoryName,
                                                       ScheduleTime = DateTime.Parse(scheduleSlot.ScheduleTime),
                                                       ScheduleTimeBefore5Min = DateTime.Parse(scheduleSlot.ScheduleTime).AddMinutes(-5),
                                                       EndTime = DateTime.Parse(scheduleSlot.EndTime)
                                                   }).ToList();

                    if (getScheduleSlot.Count > 0)
                    {
                        var scheduleEndTimeSlot = getScheduleSlot.Where(x => x.EndTime == currentTime).FirstOrDefault();

                        if (scheduleEndTimeSlot != null)
                        {
                            var getUser = GetUserDetailById(Convert.ToInt32(scheduleEndTimeSlot.UserId));
                            if (!string.IsNullOrEmpty(getUser.DeviceId))
                            {
                                var userToken = getUser.DeviceId == null ? "" : getUser.DeviceId;
                                if (userToken.Contains(','))
                                {
                                    var getUserToken = getUser.DeviceId.Split(',');
                                    foreach (var token in getUserToken)
                                    {
                                        SendIhpone(token, "\"Type\":\"" + "EndTime" + "\",\"AppName\":\"" + scheduleEndTimeSlot.AppName + "\",\"AppId\":\"" + scheduleEndTimeSlot.AppId + "\",\"CategoryId\":\"" + scheduleEndTimeSlot.CategoryId + "\"", "Your schedule time for " + scheduleEndTimeSlot.AppName + " has Ended now. Please Enter Your Spend Hour For " + scheduleEndTimeSlot.CategoryName + ".");
                                    }
                                }
                                else
                                {
                                    SendIhpone(getUser.DeviceId, "\"Type\":\"" + "EndTime" + "\",\"AppName\":\"" + scheduleEndTimeSlot.AppName + "\",\"AppId\":\"" + scheduleEndTimeSlot.AppId + "\",\"CategoryId\":\"" + scheduleEndTimeSlot.CategoryId + "\"", "Your schedule time for " + scheduleEndTimeSlot.AppName + " has Ended now. Please Enter Your Spend Hour For " + scheduleEndTimeSlot.CategoryName + ".");
                                }

                            }
                        }


                    }
                }

            }

        }

        public void SendNotificationForGoalAchievements()
        {
            DateTime now = DateTime.Now;
      
            var monthSt = new DateTime(now.Year, now.Month, 1);
            var monthEnd = monthSt.AddMonths(1).AddDays(-1);
      
            int noOfWeeks = GetWeeksCovered(monthSt, monthEnd);


            var getScheduleList = (from schedule in DB.Schedules
                                   join cate in DB.ScheduleCategoryTimes on schedule.Id equals cate.ScheduleId
                                   select new UserScheduleClass
                                   {
                                       ScheduleId = schedule.Id,
                                       CategoryId = Convert.ToInt32(cate.CategoryId),
                                       UserId = Convert.ToInt32(schedule.UserId),
                                       ScheduleStartDate = Convert.ToDateTime(schedule.DATE).ToShortDateString()
                                   }).OrderByDescending(x=>x.ScheduleId).ToList();

            List<UserScheduleClass> userScheduleList = new List<UserScheduleClass>();
            foreach (var sdl in getScheduleList)
            {
                UserScheduleClass objSchedule = new UserScheduleClass();
                if (userScheduleList.Exists(e => e.UserId == sdl.UserId) == false)
                {
                    var getScheduleData = getScheduleList.Where(x => x.UserId == sdl.UserId && x.ScheduleId==sdl.ScheduleId).OrderByDescending(y => y.ScheduleId).ToList();
                    //objSchedule.ScheduleId = getScheduleData.ScheduleId;
                    //objSchedule.UserId = getScheduleData.UserId;
                    //objSchedule.ScheduleStartDate = getScheduleData.ScheduleStartDate;
                    //userScheduleList.Add(objSchedule);
                    userScheduleList.AddRange(getScheduleData);
                }

            }


            foreach (var goalHour in userScheduleList)
            {
                
                var getCategoryLogHoursList = (from lt in DB.AppsHourForDates
                                               join
                                                   cate in DB.CategoryMasters on lt.CategoryId equals cate.Id
                                               where
                                                   lt.UserId == goalHour.UserId && 
                                                  Convert.ToDateTime(lt.Date).Date >= Convert.ToDateTime(monthSt).Date
                                                 && Convert.ToDateTime(lt.Date).Date <= Convert.ToDateTime(monthEnd).Date
                                               select new
                                               { 
                                                   UserId=goalHour.UserId,
                                                   CategoryId = Convert.ToInt32(lt.CategoryId),
                                                   CategoryName = cate.CategoryName,
                                                   Hours = GetTotalHoursInMin(Convert.ToInt32(lt.CategoryId), goalHour.UserId, Convert.ToDateTime(monthSt).ToString("dd MMMM, yyyy"), Convert.ToDateTime(monthEnd).ToString("dd MMMM, yyyy"))
                                               }).OrderBy(x => x.CategoryId).Distinct().ToList();
               var getTotalHourForLevel =getCategoryLogHoursList.Where(x => x.CategoryId == goalHour.CategoryId).FirstOrDefault();
                string achieveHours=getTotalHourForLevel !=null? getTotalHourForLevel.Hours:"";
                var getLevel = !string.IsNullOrEmpty(achieveHours) ? GetAchievementLevel(achieveHours) : "";
                if (!string.IsNullOrEmpty(getLevel) && achieveHours != "Level0")
                {
                    var getUser = GetUserDetailById(Convert.ToInt32(goalHour.UserId));

                    var userToken = getUser.DeviceId == null ? "" : getUser.DeviceId;
                    if (!string.IsNullOrEmpty(getUser.DeviceId))
                    {
                        if (userToken.Contains(','))
                        {
                            var getUserToken = getUser.DeviceId.Split(',');
                            foreach (var token in getUserToken)
                            {

                                SendIhpone(token, "\"Type\":\"" + "GoalAchievement" + "\",\"Level\":\"" + getLevel + "\",\"CategoryName\":\"" + getTotalHourForLevel.CategoryName + "\"", "You have achieved " + getLevel + " in " + getTotalHourForLevel.CategoryName + ".");
                            }
                        }
                        else
                        {
                            SendIhpone(getUser.DeviceId, "\"Type\":\"" + "GoalAchievement" + "\",\"Level\":\"" + getLevel + "\",\"CategoryName\":\"" + getTotalHourForLevel.CategoryName + "\"", "You have achieved " + getLevel + " in " + getTotalHourForLevel.CategoryName + ".");
                        }
                    }
                }
            }
        }

        public void UserLogout(int userId, string deviceId)
        {
            
            var userDetails = (from user in DB.UserMasters where user.Id == userId select user).SingleOrDefault();

            if (userDetails != null)
            {
                var userDeviceToken=userDetails.DeviceId==null?"":userDetails.DeviceId;
                if(userDeviceToken.Contains(',')){
                    var result = string.Join(", ", from v in userDeviceToken.Split(',')
                                                   where v.Trim() != deviceId
                                               select v);
                    userDetails.DeviceId = result;
                }
                else
                {
                    if (userDeviceToken.Contains(deviceId))
                    {
                        userDetails.DeviceId = "";
                    }
           
                }
                DB.SubmitChanges();
            }
        }
    }

  public class PostUserCategoryDto
  {
      public int UserId { get; set; }
      //public int CategoryId { get; set; }

      public List<int> SubCategoryIds { get; set; }

      
  }

  public class SubCategoryDto
  {
      public int SubCategoyId { get; set; }
  }

  public class UserProfileDto
  {
      public string Name { get; set; }
      public string Level { get; set; }
      public string ProfileImage { get; set; }
      public string Password { get; set; }
      public int NoOfDaysUsingApp { get; set; }

      public string Gender { get; set; }
      public string BirthDay { get; set; }
      public string Latitude { get; set; }

      public string Longitude { get; set; }
      public List<UserCategoryListDto> Categories { get; set; }
      
      public SetGoalClass SetGoalDuringWeek { get; set; }
      
  }

  public class SetGoalClass
  {
      
      public string Weekly { get; set; }

      public string Daily { get; set; }

      public List<SetGoalCategoryListClass> CategoryWithDaysList { get; set; }
  }
  public class ApplicationDto
  {
      public int Id { get; set; }
      public string AppId { get; set; }
      public string AppName { get; set; }
      public string AppLink { get; set; }
      public string appAppStoreLink { get; set; }
  }

    public class SavePostScheduleTimeDto
    {
        public int ScheduleId { get; set; }
        public int UserId{get;set;}
        public string DTime{get;set;}
        public string ScheduleDate{get;set;}
        public string Session{get;set;}

        public List<CategoryWithTimeDto> CategoryWithTime{get;set;}
        public List<SelectedAppsDto> SelectedApps{get;set;}
    }


    public class CategoryWithTimeDto{
        public int CategoryId{get;set;}

        public string Time{get;set;}
    }

    public class SelectedAppsDto{
        public int AppId{get;set;}
    }

    public class CategoryListDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryImage { get; set; }

        public List<SubCategoryListDto> SubCategory { get; set; }

        public bool IsSelected { get; set; }
    }

    public class SubCategoryListDto
    {
        public string SubCategoryName { get; set; }
        public int SubCategoryId { get; set; }
        public bool IsSelected { get; set; }
    }

     public class UserCategoryListDto
    {
         public int CategoryId{get;set;}
        public string CategoryName { get; set; }

        public string CategoryImage { get; set; }
    }

     public class AwardCategoryClass
     {
         public int CategoryId { get; set; }
         public string CategoryName { get; set; }
         public string Level { get; set; }
         public int hours { get; set; }
         public string GoalMeet { get; set; }
     }

    public class SaveWeeklyGoalDto{
        public int UserId{get;set;}
        
        public string Weekly{get;set;}

        public string Daily{get;set;}


        public List<CategoryWithDaysDto> CategoryWithDays { get; set; }
    }

    public class CategoryWithDaysDto
    {
        public int CategoryId { get; set; }
        public int NoOfDays { get; set; }
    }

    public class SetGoalCategoryListClass:CategoryWithDaysDto{
        public string CategoryName{get;set;}
    }

    public class ImpactFactorDto
    {
        public int UserId { get; set; }
        public string Type { get; set; }

        public string StartTime { get; set; }
        public string Duration { get; set; }
    }

    public class CategoryDto
    {
        public int CategoryId { get; set; }
        public List<int> SubCategoryId { get; set; }
    }
    public class CategoryDto2
    {       
        public List<int> SubCategoryId { get; set; }
    }

    public class DashboardDto
    {
        public string Level { get; set; }

        public string ScheduleStartTime { get; set; }
        public string ProfileImage { get; set; }

        public string UserAward { get; set; }

        public string Duration { get; set; }
        public List<UserScheduleDto> UserSchedules { get; set; }
        public List<UserScheduleDto> YesterdaySchedules { get; set; }
        public List<ImpactFactorDto> UserImpactFactors { get; set; }
    }

    public class UserScheduleDto
    {
        public int ScheduleId { get; set; }

        public int AppId { get; set; }
        public string AppName { get; set; }
        public string AppLogo { get; set; }
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string ScheduleTime { get; set; }

        public string EndTime { get; set; }
        public string ScheduleType { get; set; }
        public string Session { get; set; }

        public string SpendCategoryHours { get; set; }
    }

 
    public class ScheduleDetails
    {
        public string ScheduleStartTime { get; set; }
        public List<UserScheduleDto> TodaySchedules { get; set; }
        public List<UserScheduleDto> YesterdaySchedules { get; set; }
        public List<ImpactFactorDto> UserImpactFactors { get; set; }
    }

    public class UserAwardDto
    {
        public int TotalDays { get; set; }

        public string Level { get; set; }
        public List<AwardCategoryClass> caterogyArray { get; set; }
    }


    public class SubCategoryList
    {
        public int SubCateId { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubCategoryImage { get; set; }
        public DateTime CreatedDate { get; set; }
    }


    public class UserListDto
    {
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public string Name { get; set; }
        
        public string ProfileImage { get; set; }
        public string UserPassword { get; set; }
        public string UserName { get; set; }

        public string Gender { get; set; }
        public string BirthDay { get; set; }
        public string Latitude { get; set; }

        public string Longitude { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    
    public class ApplicationListDto{
        public int Id{get;set;}
        public string AppId{get;set;}

        public string CategoryName{get;set;}

        public string AppName{get;set;}

        public string AppLink{get;set;}

        public string AppStoreLink{get;set;}
        public DateTime CreatedDate { get; set; }
    }

    public class ApplicationMasterWithCat : ApplicationMaster
    {
        public string CategoryName { get; set; }
    }


    public class SchduleClass:UserScheduleDto
    {
        public int Id { get; set; }

        public System.Nullable<int> UserId { get; set; }

        public string Session { get; set; }

        public string DATE { get; set; }
        public string ShortDATE { get; set; }

        public string DTime { get; set; }

        public System.Nullable<System.DateTime> CreatedDate { get; set; }

        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
    }

    public class EmptyHourListClass
    {
        public List<string> EmptyDateArray { get; set; }
    }

    public class EnterAppsHoursClass
    {
        public int AppId { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public string SpendTime { get; set; }
        public string Date { get; set; }
    }

    public class ScheduleDateClass
    {
        public string Date { get; set; }
    }

    public class CategoryAppListClass
    {
        public List<ApplicationDto> UserApps { get; set; }
        public List<ApplicationDto> CategoryApps { get; set; }

    }

    public class UserStaticsClass
    {
        public string Month { get; set; }
        public string Date { get; set; }

        public string Day { get; set; }
        public string TotalHour { get; set; }

        public string AvgDaySpentWeekly { get; set; }

        public string AvgTimeSpentDaily { get; set; }
        public string TopActivityName { get; set; }

        public string TopActivityHourMonthly { get; set; }
        public string AvgTopActivityDaySpentWeekly { get; set; }

        public string AvgTopActivitySpentDaily { get; set; }
        public List<UserCategoryStaticsClass> CategoryArray { get; set; }
    }

    public class UserCategoryStaticsClass
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public string Hours { get; set; }

        public List<string> SelectedCategoryAppsArray { get; set; }
    }

    public class UserScheduleClass
    {
        public int ScheduleId { get; set; }
        public int CategoryId { get; set; }
        public int UserId { get; set; }
        public string ScheduleStartDate { get; set; }

    }
}

