﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UTimeNotificationServices
{
    public partial class Form1 : Form
    {
        public Form1(string[] args)
        {
            InitializeComponent();
            if ((args.Count() > 0))
            {
                if ((args[0].Trim().ToUpper() == "AUTORUN"))
                {
                    SendAchievementNotification();
                    Application.Exit();
                    System.Environment.Exit(1);
                }
                if ((args[0].Trim().ToUpper() == "UPCOMING"))
                {
                    SendUpcomingScheduleNotification();
                    Application.Exit();
                    System.Environment.Exit(1);
                }
                if ((args[0].Trim().ToUpper() == "ENDTIME"))
                {
                    SendEndTimeScheduleNotification();
                    Application.Exit();
                    System.Environment.Exit(1);
                }
                if ((args[0].Trim().ToUpper() == "LAST7DAYSINACTIVE"))
                {
                    SendNotificationForLast7DaysInActive();
                    Application.Exit();
                    System.Environment.Exit(1);
                }
            }
        }
        public void SendAchievementNotification()
        {

            var url = "http://54.81.231.35:8002/api/SendAchievementNotification";
            using (var wc = new WebClient())
            {
                var response = wc.DownloadString(url);
            }

        }

        public void SendUpcomingScheduleNotification()
        {

            var url = "http://54.81.231.35:8002/api/UpcomingSchedule";
            using (var wc = new WebClient())
            {
                var response = wc.DownloadString(url);
            }

        }

        public void SendEndTimeScheduleNotification()
        {

            var url = "http://54.81.231.35:8002/api/ScheduleEndTime";
            using (var wc = new WebClient())
            {
                var response = wc.DownloadString(url);
            }

        }

        public void SendNotificationForLast7DaysInActive()
        {

            var url = "http://54.81.231.35:8002/api/InActiveUserFromLast7Days";
            using (var wc = new WebClient())
            {
                var response = wc.DownloadString(url);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
