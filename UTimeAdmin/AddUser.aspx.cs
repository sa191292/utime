﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using System.IO;

namespace UTimeAdmin
{
    public partial class AddUser : System.Web.UI.Page
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["ImagePath"];
        protected void Page_Load(object sender, EventArgs e)
        {
            txtDob.Attributes.Add("Readonly", "true");
            if (!IsPostBack)
            {
                FillUser();
                FillRole();
            }
        }

        public bool IsValidFile(ref System.Web.UI.WebControls.FileUpload FileName)
        {
            bool functionReturnValue = false;
            string strExtension = null;
            strExtension = Path.GetExtension(FileName.PostedFile.FileName).ToLower().Trim();

            if ((strExtension.ToLower().Trim() == ".jpg" | strExtension.ToLower().Trim() == ".jpeg" | strExtension.ToLower().Trim() == ".gif" | strExtension.ToLower().Trim() == ".bmp" | strExtension.ToLower().Trim() == ".png" | strExtension.ToLower().Trim() == ".JPG" | strExtension.ToLower().Trim() == ".JPEG" | strExtension.ToLower().Trim() == ".GIF" | strExtension.ToLower().Trim() == ".BMP" | strExtension.ToLower().Trim() == ".PNG"))
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }

            return functionReturnValue;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strImageName = "";
            string strExtension = "";
            string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "..\\UTimeAPI\\ApplicationImages\\";

            if (flProfileImg.HasFile)
            {
                try
                {
                    strExtension = System.IO.Path.GetExtension(flProfileImg.PostedFile.FileName);
                    strImageName = Guid.NewGuid().ToString() + strExtension;//System.IO.Path.GetFileNameWithoutExtension(flUpload.PostedFile.FileName) + strExtension;
                    if (IsValidFile(ref flProfileImg))
                    {
                        flProfileImg.PostedFile.SaveAs(path + strImageName);

                    }
                }
                catch { }
            }
            MasterUpdate objmaster = new MasterUpdate();

            objmaster.SaveUserDetails(Convert.ToInt32(hdnUserId.Value), Convert.ToInt32(drpRole.SelectedValue), txtUName.Text, txtPwd.Text, txtDob.Text, ddlGender.SelectedValue, txtLat.Text, txtLong.Text, strImageName, true, "", txtName.Text, "","","");
            hdnUserId.Value = "0";
            txtUName.Text = "";
            txtPwd.Text = "";
            txtConfirmPassword.Text = "";
            txtDob.Text = "";
            txtLong.Text = "";
            txtLat.Text = "";
            txtName.Text = "";
            dvAddUser.Visible = false;
            dvUserList.Visible = true;
            drpRole.SelectedValue = "-1";
            rfvConfirmPassword.Enabled = true;
            NewPasswordCompare.Enabled = true;
            //Response.Write("<script>alert('Successfully Saved')</script>");
            FillUser();


        }


        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            dvAddUser.Visible = true;
            dvUserList.Visible = false;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddUser.aspx");
        }


        protected void GrdUserList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MasterUpdate objMasterUpdate = new MasterUpdate();
            if (e.CommandName == "EditItem")
            {

                var getUser = objMasterUpdate.GetUserDetailById(Convert.ToInt32(e.CommandArgument));
                if (getUser != null)
                {
                    hdnUserId.Value = getUser.Id.ToString();
                    txtUName.Text = getUser.UserEmail;
                    txtPwd.Text = getUser.UserPassword;
                    txtConfirmPassword.Text = getUser.UserPassword;
                    txtDob.Text = getUser.Dob;
                    txtLong.Text = getUser.Longitude;
                    txtLat.Text = getUser.Latitude;
                    txtName.Text = getUser.Name;
                    rfvConfirmPassword.Enabled = false;
                    NewPasswordCompare.Enabled = false;
                    drpRole.SelectedValue = Convert.ToString(getUser.RoleId);
                    ddlGender.SelectedValue = !string.IsNullOrEmpty(getUser.Gender) ? getUser.Gender.ToLower() : "male";
                }
                dvUserList.Visible = false;
                dvAddUser.Visible = true;
            }
            if (e.CommandName == "DeleteItem")
            {
                objMasterUpdate.DeleteUserById(Convert.ToInt32(e.CommandArgument));
                FillUser();
            }
        }
        protected void GrdUserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdUserList.PageIndex = e.NewPageIndex;
            FillUser();
        }
        public void FillUser()
        {
            MasterUpdate objMaster = new MasterUpdate();
            GrdUserList.DataSource = objMaster.GetAllUserList();
            GrdUserList.DataBind();

        }

        public void FillRole()
        {
            MasterUpdate objMaster = new MasterUpdate();
            drpRole.DataSource = objMaster.GetAllRole();
            drpRole.DataTextField = "RoleName";
            drpRole.DataValueField = "Id";
            drpRole.DataBind();
            drpRole.Items.Insert(0, new ListItem("Select Role", "-1"));

        }

        protected void GrdUserList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgProfile = (Image)e.Row.FindControl("imgProfile");
                string getProfilrImg =Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ProfileImage"));

                if (string.IsNullOrEmpty(getProfilrImg))
                {
                    imgProfile.ImageUrl = path + "No-Found.jpg";

                }
               
            }
        }
    }
}