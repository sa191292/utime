﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;


namespace UTimeAdmin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
       
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            MasterUpdate masterobj = new MasterUpdate();
            var obj = masterobj.Login(txtEmail.Text, txtPassword.Text,"",1,"");

            if (obj != null && obj.RoleId == 1)
            {
                Session.Timeout = 20;
                userInfo info = new userInfo();
                info.UserID = obj.Id;
                info.FullName = obj.Name;
                info.Email = obj.UserEmail;
                LoginSession.userInfo = info;
                Response.Redirect("AddUser.aspx");
            }

            else
            {
                ltrMsg.Text = "UserName and Password is not Matched";
                //Response.Write("<script>alert('UserName and Password is not Matched')</script>");
            }
        }
    }
}