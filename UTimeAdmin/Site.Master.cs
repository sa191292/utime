﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;


namespace UTimeAdmin
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (LoginSession.userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }

            else
            {
                //if (LoginSession.isadmin)
                //{
                //    ulmanageadmin.Visible = false;
                //}

                //else
                //{
                    int roleid = LoginSession.userInfo.User;
                    ltUser.Text = LoginSession.userInfo.FullName;

                    //if (roleid == 1 || roleid == 6)
                    //{
                    //    ulmanageadmin.Visible = true;
                    //}

                    //else if (roleid == 5)
                    //{
                    //    ulmanageadmin.Visible = false;
                    //}
                //}

                Session.Timeout = 20;
            }

        }

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();

            LoginSession.userInfo = null;
            Response.Redirect("Login.aspx");
        }
    }

}