﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;

namespace UTimeAdmin
{
    public partial class AddSubCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillSubCategory();
                FillCategory();
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            MasterUpdate objmaster = new MasterUpdate();

            objmaster.SaveSubCategoryDetails(Convert.ToInt32(hdSubCateId.Value),Convert.ToInt32(drpCate.SelectedValue),txtSubCateName.Text);
            hdSubCateId.Value = "0";
            txtSubCateName.Text = "";
            dvAddSubCategory.Visible = false;
            dvSubCategoryList.Visible = true;
            drpCate.SelectedValue = "-1";
            //Response.Write("<script>alert('Successfully Saved')</script>");
            FillSubCategory();


        }


        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            dvAddSubCategory.Visible = true;
            dvSubCategoryList.Visible = false;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddSubCategory.aspx");
        }


        protected void GrdSubCategoryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MasterUpdate objMasterUpdate = new MasterUpdate();
            if (e.CommandName == "EditItem")
            {

                var getSubCate = objMasterUpdate.GetSubCategoryById(Convert.ToInt32(e.CommandArgument));

                txtSubCateName.Text = getSubCate.SubCategoryName;
                hdSubCateId.Value = getSubCate.Id.ToString();
                drpCate.SelectedValue = getSubCate.CategoryId.ToString();
                dvSubCategoryList.Visible = false;
                dvAddSubCategory.Visible = true;
            }
            if (e.CommandName == "DeleteItem")
            {
                objMasterUpdate.DeleteSubCategory(Convert.ToInt32(e.CommandArgument));
                FillSubCategory();
            }
        }
        protected void GrdSubCategoryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdSubCategoryList.PageIndex = e.NewPageIndex;
            FillSubCategory();
        }
        public void FillSubCategory()
        {
            MasterUpdate objMaster = new MasterUpdate();
            GrdSubCategoryList.DataSource = objMaster.GetAllSubCategories();
            GrdSubCategoryList.DataBind();

        }

        public void FillCategory()
        {
            MasterUpdate objMaster = new MasterUpdate();
            drpCate.DataSource = objMaster.GetAllCategoryList();
            drpCate.DataTextField = "CategoryName";
            drpCate.DataValueField = "Id";
            drpCate.DataBind();
            drpCate.Items.Insert(0, new ListItem("Select Category", "-1"));

        }
    }
}