﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddApplication.aspx.cs" Inherits="UTimeAdmin.AddApplication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="row wrapper border-bottom white-bg page-heading">

        <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h2>Application Manager</h2>
                        
                    </div>
                    <div class="ibox-content" id="dvAddApplication" runat="server" visible="false">
                        <asp:HiddenField ID="hdAppId" runat="server" Value="0" />
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="m-t-none m-b">Add/Edit Application</h3>
                    <asp:ValidationSummary ID="val" runat="server" ShowMessageBox="true" ValidationGroup="Save"
                        ShowSummary="false" />             

                                   <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Category Name :</label>

                                    <div class="col-sm-6">
                                          <asp:DropDownList ID="drpCate" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpCate_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvCate" runat="server" ErrorMessage="Please select Category" InitialValue="-1" ControlToValidate="drpCate"  ValidationGroup="Save">*</asp:RequiredFieldValidator>
                        
                                        <br />
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SubCategories:</label>

                                    <div class="col-sm-6">
                                         
                                       <asp:ListBox ID="lsSubCategories" runat="server" class="form-control" SelectionMode="Multiple">
                                                    </asp:ListBox>
                                                    <%--<asp:RequiredFieldValidator ID="rfvSubCategories" runat="server" 
                                                        ValidationGroup="Save" ControlToValidate="lsSubCategories" Text="*" ErrorMessage="Please select subCategory!"></asp:RequiredFieldValidator> --%>
                        
                                        <br />
                                       
                                    </div>
                                </div>
                               <%-- <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">AppId :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtAppId" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAppId" runat="server" ErrorMessage="Please Enter Application ID" ControlToValidate="txtAppId" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>--%>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Application Name :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtAppName" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAppName" runat="server" ErrorMessage="Please Enter Application Name" ControlToValidate="txtAppName" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">App Link :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtAppLink" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAppLink" runat="server" ErrorMessage="Please Enter Application Link" ControlToValidate="txtAppLink" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">App Store Link :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtAppStoreLink" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAppStoreLink" runat="server" ErrorMessage="Please Enter App Store Link" ControlToValidate="txtAppStoreLink" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                            
                                
                                <div class="form-group row" style="float:right">
                                    
                                     <asp:Button ID="btnCancel" Text="Cancel" ValidationGroup="Save" runat="server" OnClick="btnCancel_Click"
                        class="btn btn-white btn-sm"  CausesValidation="False" />
                   
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnSave" Text="Save Changes" ValidationGroup="Save" runat="server" OnClick="btnSave_Click"
                        class="btn btn-primary btn-sm"  />
                                    
                                </div>    
                               </div>
                            </div>
                        </div>   
                    
                   <div class="col-lg-12" id="dvAppList" runat="server" > 
                
                <div class="col-lg-12">
                
                    <div class="ibox-title">
                        <h5>Application List</h5>
                         
                        <h5 style="float:right"><asp:LinkButton ID="lnkAddApp" runat="server" OnClick="lnkAdd_Click">Add Application</asp:LinkButton></h5>
                    
                    </div>
                   
                    <div class="ibox-content">

                         <asp:GridView ID="GrdAppList" runat="server" AutoGenerateColumns="false" Width="100%"
                    PageSize="10" AllowPaging="true" OnPageIndexChanging="GrdAppList_PageIndexChanging"
                    OnRowCommand="GrdAppList_RowCommand" CssClass="table table-bordered">
                    <RowStyle HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Category Name" DataField="CategoryName" />
                        <asp:BoundField HeaderText="AppID" DataField="AppId" />
                        <asp:BoundField HeaderText="Application Name" DataField="AppName" />
                        <asp:BoundField HeaderText="App Link" DataField="AppLink" />
                        <asp:BoundField HeaderText="App Store Link" DataField="AppStoreLink" /> 
                         
                         <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <%#string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkedit" runat="server" Text="Edit" CommandName="EditItem" CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>/
                            <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete" OnClientClick="return confirm('Do you want to Delete this Record?');" CommandArgument='<%#Eval("Id") %>' CommandName="DeleteItem"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                    
                    </div>
                
            </div>
        </div> 
                        
               </div>
       </div>
            
       
    </section>
</asp:Content>
