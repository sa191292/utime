﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
namespace UTimeAdmin
{
    public partial class AddCategory : System.Web.UI.Page
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["ImagePath"];
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillCategory();
            }
        }



        public bool IsValidFile(ref System.Web.UI.WebControls.FileUpload FileName)
        {
            bool functionReturnValue = false;
            string strExtension = null;
            strExtension = Path.GetExtension(FileName.PostedFile.FileName).ToLower().Trim();

            if ((strExtension.ToLower().Trim() == ".jpg" | strExtension.ToLower().Trim() == ".jpeg" | strExtension.ToLower().Trim() == ".gif" | strExtension.ToLower().Trim() == ".bmp" | strExtension.ToLower().Trim() == ".png" | strExtension.ToLower().Trim() == ".JPG" | strExtension.ToLower().Trim() == ".JPEG" | strExtension.ToLower().Trim() == ".GIF" | strExtension.ToLower().Trim() == ".BMP" | strExtension.ToLower().Trim() == ".PNG"))
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }

            return functionReturnValue;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string strImageName = "";
            string strExtension = "";
            string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath.ToString() + "..\\UTimeAPI\\ApplicationImages\\";

            if (flCateImg.HasFile)
            {
                try
                {
                    strExtension = System.IO.Path.GetExtension(flCateImg.PostedFile.FileName);
                    strImageName = Guid.NewGuid().ToString() + strExtension;//System.IO.Path.GetFileNameWithoutExtension(flUpload.PostedFile.FileName) + strExtension;
                    if (IsValidFile(ref flCateImg))
                    {
                        flCateImg.PostedFile.SaveAs(path + strImageName);

                    }
                }
                catch { }
            }
            MasterUpdate objmaster = new MasterUpdate();

            objmaster.AddCategory(Convert.ToInt32(hdCateId.Value), txtCateName.Text, strImageName);
            hdCateId.Value = "0";
            txtCateName.Text = "";
            dvAddCategory.Visible = false;
            dvCategoryList.Visible = true;
            //Response.Write("<script>alert('Successfully Saved')</script>");
            FillCategory();


        }


        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            dvAddCategory.Visible = true;
            dvCategoryList.Visible = false;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCategory.aspx");
        }


        protected void GrdCategoryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MasterUpdate objMasterUpdate = new MasterUpdate();
            if (e.CommandName == "EditItem")
            {

                var getCate = objMasterUpdate.GetCategoryById(Convert.ToInt32(e.CommandArgument));

                txtCateName.Text=getCate.CategoryName;
                hdCateId.Value = getCate.Id.ToString();
                dvCategoryList.Visible = false;
                dvAddCategory.Visible = true;
            }
            if (e.CommandName == "DeleteItem")
            {
                objMasterUpdate.DeleteCategory(Convert.ToInt32(e.CommandArgument));
                FillCategory();
            }
        }
        protected void GrdCategoryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdCategoryList.PageIndex = e.NewPageIndex;
            FillCategory();
        }
        public void FillCategory()
        {
            MasterUpdate objMaster = new MasterUpdate();
            GrdCategoryList.DataSource = objMaster.GetAllCategoryList();
            GrdCategoryList.DataBind();

        }
    }
}