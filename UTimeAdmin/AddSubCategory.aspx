﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddSubCategory.aspx.cs" Inherits="UTimeAdmin.AddSubCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="row wrapper border-bottom white-bg page-heading">

        <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h2>SubCategory Manager</h2>
                        
                    </div>
                    <div class="ibox-content" id="dvAddSubCategory" runat="server" visible="false">
                        <asp:HiddenField ID="hdSubCateId" runat="server" Value="0" />
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="m-t-none m-b">Add/Edit SubCategory</h3>
                    <asp:ValidationSummary ID="val" runat="server" ShowMessageBox="true" ValidationGroup="Save"
                        ShowSummary="false" />             

                                   <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Category Name :</label>

                                    <div class="col-sm-6">
                                          <asp:DropDownList ID="drpCate" runat="server" class="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvCate" runat="server" ErrorMessage="Please select Category" InitialValue="-1" ControlToValidate="drpCate"  ValidationGroup="Save">*</asp:RequiredFieldValidator>
                        
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SubCategory Name :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtSubCateName" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSubCategory" runat="server" ErrorMessage="Please Enter SubCategory Name" ControlToValidate="txtSubCateName" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                
                                <div class="form-group row" style="float:right">
                                    
                                     <asp:Button ID="btnCancel" Text="Cancel" ValidationGroup="Save" runat="server" OnClick="btnCancel_Click"
                        class="btn btn-white btn-sm"  CausesValidation="False" />
                   
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnSave" Text="Save Changes" ValidationGroup="Save" runat="server" OnClick="btnSave_Click"
                        class="btn btn-primary btn-sm"  />
                                    
                                </div>    
                               </div>
                            </div>
                        </div>   
                    
                   <div class="col-lg-12" id="dvSubCategoryList" runat="server" > 
                
                <div class="col-lg-12">
                
                    <div class="ibox-title">
                        <h5>SubCategory List</h5>
                         
                        <h5 style="float:right"><asp:LinkButton ID="lnkAddSubCate" runat="server" OnClick="lnkAdd_Click">Add SubCategory</asp:LinkButton></h5>
                    
                    </div>
                   
                    <div class="ibox-content">

                         <asp:GridView ID="GrdSubCategoryList" runat="server" AutoGenerateColumns="false" Width="100%"
                    PageSize="10" AllowPaging="true" OnPageIndexChanging="GrdSubCategoryList_PageIndexChanging"
                    OnRowCommand="GrdSubCategoryList_RowCommand" CssClass="table table-bordered">
                    <RowStyle HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Category Name" DataField="CategoryName" />
                        
                         <asp:BoundField HeaderText="SubCategory Name" DataField="SubCategoryName" />
                         <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <%#string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkedit" runat="server" Text="Edit" CommandName="EditItem" CommandArgument='<%#Eval("SubCateId") %>'></asp:LinkButton>/
                            <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete" OnClientClick="return confirm('Do you want to Delete this Record?');" CommandArgument='<%#Eval("SubCateId") %>' CommandName="DeleteItem"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                    
                    </div>
                
            </div>
        </div> 
                        
               </div>
       </div>
            
       
    </section>
</asp:Content>
