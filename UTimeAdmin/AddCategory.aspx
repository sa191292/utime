﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="UTimeAdmin.AddCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="row wrapper border-bottom white-bg page-heading">


        <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h2>Category Manager</h2>
                        
                    </div>
                    <div class="ibox-content" id="dvAddCategory" runat="server" visible="false">
                        <asp:HiddenField ID="hdCateId" runat="server" Value="0" />
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="m-t-none m-b">Add/Edit Category</h3>
                    <asp:ValidationSummary ID="val" runat="server" ShowMessageBox="true" ValidationGroup="Save"
                        ShowSummary="false" />             
                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Category Name:</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtCateName" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCategory" runat="server" ErrorMessage="Please Enter Category Name" ControlToValidate="txtCateName" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Category Image:</label>

                                    <div class="col-sm-6">
                                        <asp:FileUpload ID="flCateImg" runat="server" class="form-control" />

                                        <br />
                                    </div>
                                </div>
                                
                                <div class="form-group row" style="float:right">
                                    
                                     <asp:Button ID="btnCancel" Text="Cancel" ValidationGroup="Save" runat="server" OnClick="btnCancel_Click"
                        class="btn btn-white btn-sm"  CausesValidation="False" />
                   
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnSave" Text="Save Changes" ValidationGroup="Save" runat="server" OnClick="btnSave_Click"
                        class="btn btn-primary btn-sm"  />
                                    
                                </div>    
                               </div>
                            </div>
                        </div>   
                    
                   <div class="col-lg-12" id="dvCategoryList" runat="server" > 
                
                <div class="col-lg-12">
                
                    <div class="ibox-title">
                        <h5>Category List</h5>
                         
                        <h5 style="float:right"><asp:LinkButton ID="lnkAddCity" runat="server" OnClick="lnkAdd_Click">Add Category</asp:LinkButton></h5>
                    
                    </div>
                   
                    <div class="ibox-content">

                         <asp:GridView ID="GrdCategoryList" runat="server" AutoGenerateColumns="false" Width="100%"
                    PageSize="10" AllowPaging="true" OnPageIndexChanging="GrdCategoryList_PageIndexChanging"
                    OnRowCommand="GrdCategoryList_RowCommand" CssClass="table table-bordered">
                    <RowStyle HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Category Name" DataField="CategoryName" />
                        <asp:TemplateField HeaderText="Category Image">
                          <ItemTemplate>
                      
                          <asp:Image ID="imgCateg" runat="server" Height="60" Width="60"   ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["ImagePath"]+ Eval("CategoryImage") %>' />
                          </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <%#string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkedit" runat="server" Text="Edit" CommandName="EditItem" CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>/
                            <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete" OnClientClick="return confirm('Do you want to Delete this Record?');" CommandArgument='<%#Eval("Id") %>' CommandName="DeleteItem"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                    
                    </div>
                
            </div>
        </div> 
                        
               </div>
       </div>
            
       
    </section>
</asp:Content>