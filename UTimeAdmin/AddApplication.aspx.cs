﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;

namespace UTimeAdmin
{
    public partial class AddApplication : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillApplication();
                FillCategory();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            MasterUpdate objmaster = new MasterUpdate();

            List<SubCategoryDto> subCategoryList = new List<SubCategoryDto>();
           
            for (int i = 0; i < lsSubCategories.Items.Count; i++)
            {
                if (lsSubCategories.Items[i].Selected)
                {
                    SubCategoryDto objSubCategory = new SubCategoryDto();
                    objSubCategory.SubCategoyId = Convert.ToInt32(lsSubCategories.Items[i].Value);
                    subCategoryList.Add(objSubCategory);
                }
            }

            objmaster.SaveApplicationDetails(Convert.ToInt32(hdAppId.Value), Convert.ToInt32(drpCate.SelectedValue), "", txtAppName.Text, txtAppLink.Text, txtAppStoreLink.Text, subCategoryList);
            hdAppId.Value = "0";
            //txtAppId.Text = "";
            txtAppName.Text = "";
            txtAppLink.Text = "";
            txtAppStoreLink.Text = "";
            dvAddApplication.Visible = false;
            dvAppList.Visible = true;
            drpCate.SelectedValue = "-1";
            //Response.Write("<script>alert('Successfully Saved')</script>");
            FillApplication();


        }


        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            dvAddApplication.Visible = true;
            dvAppList.Visible = false;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddApplication.aspx");
        }


        protected void GrdAppList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MasterUpdate objMasterUpdate = new MasterUpdate();
            if (e.CommandName == "EditItem")
            {

                var getApplication = objMasterUpdate.GetApplicationById(Convert.ToInt32(e.CommandArgument));
                if (getApplication != null)
                {
                    hdAppId.Value = getApplication.Id.ToString();
                    //txtAppId.Text = getApplication.AppId;
                    txtAppName.Text = getApplication.AppName;
                    txtAppLink.Text = getApplication.AppLink;
                    txtAppStoreLink.Text = getApplication.AppStoreLink;
                    drpCate.SelectedValue = getApplication.CategoryId.ToString();
                    FillSubCategories(Convert.ToInt32(getApplication.CategoryId));
                    var getSubCategory = objMasterUpdate.GetApplicationSubCategoryByAppId(Convert.ToInt32(getApplication.Id));

                    if (getSubCategory.Count > 0)
                    {
                        foreach (var t in getSubCategory)
                        {
                            if (lsSubCategories.Items.FindByValue(Convert.ToString(t.SubCategoryId)) != null)
                                lsSubCategories.Items.FindByValue(Convert.ToString(t.SubCategoryId)).Selected = true;
                        }
                    }
                    dvAppList.Visible = false;
                    dvAddApplication.Visible = true;
                }
            }
            if (e.CommandName == "DeleteItem")
            {
                objMasterUpdate.DeleteApplicationById(Convert.ToInt32(e.CommandArgument));
                FillApplication();
            }
        }
        protected void GrdAppList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdAppList.PageIndex = e.NewPageIndex;
            FillApplication();
        }
        public void FillApplication()
        {
            MasterUpdate objMaster = new MasterUpdate();
            GrdAppList.DataSource = objMaster.GetAllApplication();
            GrdAppList.DataBind();

        }

        public void FillCategory()
        {
            MasterUpdate objMaster = new MasterUpdate();
            drpCate.DataSource = objMaster.GetAllCategoryList();
            drpCate.DataTextField = "CategoryName";
            drpCate.DataValueField = "Id";
            drpCate.DataBind();
            drpCate.Items.Insert(0, new ListItem("Select Category", "-1"));

        }

        public void FillSubCategories(int categoryId)
        {
            MasterUpdate objMaster = new MasterUpdate();
            var allSubCategories = objMaster.GetSubCategoriesByCategoryId(categoryId);

            lsSubCategories.DataSource = allSubCategories;
            lsSubCategories.DataTextField = "SubCategoryName";
            lsSubCategories.DataValueField = "SubCateId";
            lsSubCategories.DataBind();
        }

        protected void drpCate_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSubCategories(Convert.ToInt32(drpCate.SelectedValue));
        }

    }
}