﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UTimeAdmin.Startup))]
namespace UTimeAdmin
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
