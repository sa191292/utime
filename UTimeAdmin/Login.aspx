﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="UTimeAdmin.Login" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>UTIME | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <form id="form1" runat="server">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>

                    <h6 class="logo-name">UTIME</h6>

                </div>
                <h3>Welcome to UTIME</h3>
                <%--<p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>--%>
                <p>Login to your account</p>
                <div>
                    <span style="color: red;">
                        <asp:Literal ID="ltrMsg" Text="" runat="server" /></span>
                </div>


                <div class="form-group">
                    <%--<input type="email" class="form-control" placeholder="Username" required="">--%>

                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Enter UserName"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Please Enter Your Email" ControlToValidate="txtEmail" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                </div>


                <div class="form-group">
                    <%--<input type="password" class="form-control" placeholder="Password" required="">--%>
                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Enter Password" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="password cannot be Left Blank " ControlToValidate="txtPassword" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                </div>
                <%--<button type="submit" class="btn btn-primary block full-width m-b">Login</button>--%>
                <asp:Button ID="btnLogin" runat="server" Text="Login" ValidationGroup="Save" OnClick="btnLogin_Click" class="btn btn-primary block full-width m-b" />

                <asp:ValidationSummary ID="val" runat="server" ShowMessageBox="true" ValidationGroup="Save"
                    ShowSummary="false" />



            </div>
        </div>
    
    </form>
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>
