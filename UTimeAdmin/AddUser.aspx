﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="UTimeAdmin.AddUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<section class="row wrapper border-bottom white-bg page-heading">

        <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h2>User Manager</h2>
                        
                    </div>
                    <div class="ibox-content" id="dvAddUser" runat="server" visible="false">
                        <asp:HiddenField ID="hdnUserId" runat="server" Value="0" />
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="m-t-none m-b">Add/Edit User</h3>
                    <asp:ValidationSummary ID="val" runat="server" ShowMessageBox="true" ValidationGroup="Save"
                        ShowSummary="false" />             

                                   <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Role Name :</label>

                                    <div class="col-sm-6">
                                          <asp:DropDownList ID="drpRole" runat="server" class="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvRole" runat="server" InitialValue="-1" ErrorMessage="Please select Role" ControlToValidate="drpRole"  ValidationGroup="Save">*</asp:RequiredFieldValidator>
                        
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Please Enter Name" ControlToValidate="txtName" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">User Name :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtUName" runat="server"  class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsrName" runat="server" ErrorMessage="Please Enter User Name" ControlToValidate="txtUName" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Password :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Please Enter Password" ControlToValidate="txtPwd" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Confirm Password :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ErrorMessage="Please Enter Confirm Password" ControlToValidate="txtConfirmPassword" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                       <asp:CompareValidator ID="NewPasswordCompare" runat="server" ErrorMessage="The Confirm Password must match the New Password entry."
                        ControlToValidate="txtConfirmPassword" ControlToCompare="txtPwd" ValidationGroup="Save">*</asp:CompareValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">BirthDay :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtDob" runat="server"   class="form-control date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvBirthday" runat="server" ErrorMessage="Please Enter BirthDay" ControlToValidate="txtDob" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                <div class="form-group row">
                                     <label class="col-sm-3 col-form-label">Gender :</label>

                                    <div class="col-sm-6">
                                   <asp:DropDownList ID="ddlGender" runat="server" class="form-control">
                                                 <asp:ListItem Text="Male" Value="male"></asp:ListItem>
                                                 <asp:ListItem Text="Female" Value="female"></asp:ListItem>
                                  </asp:DropDownList>
                                  
                                    </div>
                              </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Latitude :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtLat" runat="server"  class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLat" runat="server" ErrorMessage="Please Enter Latitude" ControlToValidate="txtLat" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Longitude :</label>

                                    <div class="col-sm-6">
                                         <asp:TextBox ID="txtLong" runat="server"  class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLong" runat="server" ErrorMessage="Please Enter Longitude" ControlToValidate="txtLong" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                        <br />
                                    </div>
                                </div>

                                     <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Profile Image :</label>

                                    <div class="col-sm-6">
                                        <asp:FileUpload ID="flProfileImg" runat="server" class="form-control" />

                                        <br />
                                    </div>
                                </div>
                            
                                
                                <div class="form-group row" style="float:right">
                                    
                                     <asp:Button ID="btnCancel" Text="Cancel" ValidationGroup="Save" runat="server" OnClick="btnCancel_Click"
                        class="btn btn-white btn-sm"  CausesValidation="False" />
                   
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnSave" Text="Save Changes" ValidationGroup="Save" runat="server" OnClick="btnSave_Click"
                        class="btn btn-primary btn-sm"  />
                                    
                                </div>    
                               </div>
                            </div>
                        </div>   
                    
                   <div class="col-lg-12" id="dvUserList" runat="server" > 
                
                <div class="col-lg-12">
                
                    <div class="ibox-title">
                        <h5>User List</h5>
                         
                        <h5 style="float:right"><asp:LinkButton ID="lnkAddUser" runat="server" OnClick="lnkAdd_Click">Add User</asp:LinkButton></h5>
                    
                    </div>
                   
                    <div class="ibox-content">

                         <asp:GridView ID="GrdUserList" runat="server" AutoGenerateColumns="false" Width="100%"
                    PageSize="10" AllowPaging="true" OnPageIndexChanging="GrdUserList_PageIndexChanging"
                    OnRowCommand="GrdUserList_RowCommand" OnRowDataBound="GrdUserList_RowDataBound" CssClass="table table-bordered" >
                    <RowStyle HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Role Name" DataField="RoleName" />
                        <asp:BoundField HeaderText="User Name" DataField="UserName" />
                          <asp:BoundField HeaderText="Name" DataField="Name" />
                        <asp:TemplateField HeaderText="Profile Image">
                          <ItemTemplate>
                      
                          <asp:Image ID="imgProfile" runat="server"  Height="60" Width="60"   ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["ImagePath"]+ Eval("ProfileImage") %>' />
                          </ItemTemplate>
                        </asp:TemplateField>
                         <asp:BoundField HeaderText="User Password" DataField="UserPassword" />
                         <asp:BoundField HeaderText="BirthDay" DataField="BirthDay" />
                         <asp:BoundField HeaderText="Gender" DataField="Gender" />
                         <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <%#string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkedit" runat="server" Text="Edit" CommandName="EditItem" CommandArgument='<%#Eval("UserId") %>'></asp:LinkButton>/
                            <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete" OnClientClick="return confirm('Do you want to Delete this Record?');" CommandArgument='<%#Eval("UserId") %>' CommandName="DeleteItem"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                    
                    </div>
                
            </div>
        </div> 
                        
               </div>
       </div>
            
       
    </section>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtDob.ClientID%>").datepicker({
                 showOn: "button",
                 dateFormat: "MM/dd/yy",
                 buttonImage: "Images/calendar.gif",
                 buttonImageOnly: true

             });


         });

          </script>
</asp:Content>
